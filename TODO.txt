Group Browser:

My Groups tab -
	* Allow for updating of my groups based on new Parse information on fragment creation



Map Viewer:
	* Show tracking data
	* ? Provide access to the data in null geopoints
	* Improve filter options fragment in landscape mode


Camera Activity:
	* ? Change post-photo dialog window to fragment that responds to rotation
	* Add loading indicator that indicates that the location finding is still pending.
	* ? Change location capture flow so that it can operate while description window is open.

Settings Activity:
	!* Improve log in page
	* Add warning message when enabling tracking


 * Ensure logging into Parse works well and that there are no issues if the user is not logged in at any point
 * ? Ensure group names remain unique, caps insensitive.
 !* Enable e-mail activation.










