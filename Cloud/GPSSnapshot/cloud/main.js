// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function (request, response) {
    response.success("Hello world!");


});

Parse.Cloud.afterSave("DataPoint", function (request, response) {
    if (request.object.get("image") != null) {
        var groupQuery = new Parse.Query("DataGroup");
        groupQuery.equalTo("group_name", request.object.get("group_name"));
        var titleString;
        if (request.object.has("urgent") && !request.object.get("urgent"))
        {
            titleString = "New Data Entry";
            groupQuery.equalTo("notify_urgent_only", false);
        }
        else
        {
            titleString = "[Urgent] New Data Entry";
        }

        var installQuery = new Parse.Query(Parse.Installation);
        installQuery.matchesKeyInQuery("currentuser", "admin", groupQuery);

        Parse.Push.send({
            where: installQuery,
            data: {
                title: titleString,
                alert: "New entry in group '" + request.object.get("group_name") + "'.",
                uri: "welog://activity.map/" + request.object.get("group_name") + "&" + request.object.id,
            },
        }, {
            success: function () {

            },
            failure: function (error) {

            }


        });
    }
});