package com.sprelf.gpssnapshot;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.parse.LogInCallback;
import com.parse.LogOutCallback;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.sprelf.gpssnapshot.Activities.SettingsActivity;
import com.sprelf.gpssnapshot.Handlers.DatabaseHandler;
import com.sprelf.gpssnapshot.Receivers.PictureCleanupReceiver;
import com.sprelf.gpssnapshot.Receivers.TrackingReceiver;
import com.sprelf.gpssnapshot.Receivers.UploadReceiver;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;
import com.sprelf.gpssnapshot.UtilityClasses.LogInResponse;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Chris on 13.09.2015.
 */
@ReportsCrashes(
        formKey = "",
        formUri = "https://cgdilley.cloudant.com/acra-welog/_design/acra-storage/_update/report",
        reportType = org.acra.sender.HttpSender.Type.JSON,
        httpMethod = org.acra.sender.HttpSender.Method.PUT,
        formUriBasicAuthLogin="whiessabingstaretwouteat",
        formUriBasicAuthPassword="a18f6c504554806899dbe83516267d860e6d6418"
)
public class GPSSnapshot extends Application
{


    public static String PARSE_APPID = "Vp5vkzG8k1GkIhuPK9SlB9GGGHumksMcX9MsN97O";
    public static String PARSE_CLIENTKEY = "JWCJ5pUrQ6v5EpmGsz0ha27b1DTNDvdRIXsqrtiU";

    public static String MAP_API_KEY = "AIzaSyD_3G1QygGFXfTnKuQmqS_39O5tMj6JLNk"; // DEBUG

    // These used to be hard-coded values, but these now represent the default values
    public static final int GPS_POLLING_FREQ_DEFAULT = 1000;  // in milliseconds
    public static final int TRACKING_ACCURACY_DEFAULT = 10;  // in meters
    public static final int IMAGE_GPS_ACCURACY_DEFAULT = 10; // in meters
    public static final int TRACKING_TIMEOUT_DEFAULT = 30;     // in seconds
    public static final int IMAGE_GPS_TIMEOUT_DEFAULT = 30;    // in seconds
    public static final int PICTURE_DELAY_DEFAULT = 10;  // in seconds
    public static final int INTERNET_POLLING_FREQ_DEFAULT = 30;  // in minutes
    public static final int TRACKING_POLL_FREQ_DEFAULT = 30;     // in minutes
    public static final boolean NOTIFICATIONS_ENABLED_DEFAULT = true;
    public static final boolean NOTIFICATIONS_SOUND_OVERRIDE_DEFAULT = true;
    public static final boolean ENABLE_TRACKING_DEFAULT = false;
    public static final String IMAGE_RETENTION_DEFAULT = "Forever";
    public static final String UPLOAD_TYPE_DEFAULT = "3G or WiFi";
    public static final int UPLOAD_SIZE_DEFAULT = 45;  // in KB

    private static int PICTURE_CLEANUP_FREQ = 24;  // in hours
    public static int NULL_GPS = -1;
    private static int PENDINGINTENT_UPLOAD_ID = 1;
    private static int PENDINGINTENT_TRACK_ID = 2;
    private static int PENDINGINTENT_CLEANUP_ID = 3;

    public static int IMAGE_DELETION_SUCCESSFUL = 0;
    public static int IMAGE_DELETION_NOTFOUND = 1;
    public static int IMAGE_DELETION_FAILED = 2;

    public static final int ERROR_NO_INTERNET = 1;
    public static final int ERROR_NO_LOGIN = 4;
    public static final int ERROR_BAD_LOGIN = 5;
    public static final int ERROR_INVALID_LOGIN = 6;
    public static final int ERROR_UNAUTHENTICATED_LOGIN = 7;
    public static final int ERROR_PARSE = 10;
    public static final int ERROR_INVALID_NAME = 15;
    public static final int ERROR_TAKEN_NAME = 16;
    public static final int ERROR_RESERVED_NAME = 17;
    public static final int ERROR_TEMPORARY_USER_NEW_GROUP = 18;
    public static final int ERROR_UNKNOWN = -1;

    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    @Override
    public void onCreate()
    {
        super.onCreate();

        loadPreferences();

        Parse.initialize(this, GPSSnapshot.PARSE_APPID, GPSSnapshot.PARSE_CLIENTKEY);
        ACRA.init(this);

        ParseInstallation.getCurrentInstallation().saveInBackground();
    }


    /**
     * Checks to see if settings have been previously generated, and if not, generates them
     * using the default values.
     */
    private void loadPreferences()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean("first", true))
        {
            SharedPreferences.Editor editor = prefs.edit();

            editor.putBoolean("first", false);

            editor.putString("display_name", "");

            editor.putString("tracking_freq", Integer.toString(TRACKING_POLL_FREQ_DEFAULT));
            editor.putString("tracking_accuracy", Integer.toString(TRACKING_ACCURACY_DEFAULT));
            editor.putString("tracking_timeout", Integer.toString(TRACKING_TIMEOUT_DEFAULT));
            editor.putBoolean("enable_tracking", ENABLE_TRACKING_DEFAULT);

            editor.putString("image_accuracy", Integer.toString(IMAGE_GPS_ACCURACY_DEFAULT));
            editor.putString("image_timeout", Integer.toString(IMAGE_GPS_TIMEOUT_DEFAULT));
            editor.putString("image_delay", Integer.toString(PICTURE_DELAY_DEFAULT));
            editor.putString("image_retention", IMAGE_RETENTION_DEFAULT);

            editor.putString("upload_freq", Integer.toString(INTERNET_POLLING_FREQ_DEFAULT));
            editor.putString("upload_type", UPLOAD_TYPE_DEFAULT);
            editor.putString("upload_size", Integer.toString(UPLOAD_SIZE_DEFAULT));

            editor.putBoolean("notifications_enabled", NOTIFICATIONS_ENABLED_DEFAULT);
            editor.putBoolean("notifications_sound", NOTIFICATIONS_SOUND_OVERRIDE_DEFAULT);

            editor.commit();
        }
    }

    /**
     * Retrieves the string value of a given setting.  Returns "" if key is not found.
     *
     * @param c   Context Context within which to perform the operation.
     * @param key Key representing the setting to be accessed.
     * @return Returns the value held by the given key as a String.
     */
    public static String getStringPref(Context c, String key)
    {
        return PreferenceManager.getDefaultSharedPreferences(c).getString(key, "");
    }

    /**
     * Retrieves the boolean value of a given setting.  Defaults to 'false' if the key is not found.
     *
     * @param c   Context Context within which to perform the operation.
     * @param key Key representing the setting to be accessed.
     * @return Returns the value held by the given key as a boolean.
     */
    public static Boolean getBoolPref(Context c, String key)
    {
        return PreferenceManager.getDefaultSharedPreferences(c).getBoolean(key, false);
    }

    /**
     * Retrieves the integer value of a given setting.  Defaults to '0' if key is not found.
     *
     * @param c   Context Context within which to perform the operation.
     * @param key Key representing the setting to be accessed.
     * @return Returns the value held by the given key as an integer.
     */
    public static int getIntPref(Context c, String key)
    {
        return Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(c).getString(key, "0"));
    }

    /**
     * Sets the given preference to the given value.
     *
     * @param c     Context within which to perform the operation.
     * @param key   Key representing the setting to be accessed.
     * @param value The value to be set.
     */
    public static void setBoolPref(Context c, String key, boolean value)
    {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(c).edit();
        prefs.putBoolean(key, value);
        prefs.apply();
    }

    /**
     * Sets the given preference to the given value.
     *
     * @param c     Context within which to perform the operation.
     * @param key   Key representing the setting to be accessed.
     * @param value The value to be set.
     */
    public static void setIntPref(Context c, String key, int value)
    {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(c).edit();
        prefs.putString(key, Integer.toString(value));
        prefs.apply();
    }

    /**
     * Resets all tutorials, allowing them to be shown again.
     *
     * @param c Context within which to perform the operation.
     */
    public static void resetTutorials(Context c)
    {
        SharedPreferences.Editor prefs = PreferenceManager.getDefaultSharedPreferences(c).edit();
        prefs.putBoolean("first_camera", false);
        prefs.putBoolean("first_settings", false);
        prefs.putBoolean("first_map", false);
        prefs.putBoolean("first_my_groups", false);
        prefs.putBoolean("first_browse_groups", false);
        prefs.putBoolean("first_new_group", false);
        prefs.putBoolean("first_settings_user", false);
        prefs.apply();
    }


    /**
     * Starts the alarm service for periodically attempting to upload any data in the database.
     *
     * @param c Context within which to perform the operation.
     */
    public static void startUpdateService(Context c)
    {
        startUpdateServiceAtFrequency(c, getIntPref(c, "upload_freq"));
    }

    /**
     * Starts the alarm service for periodically attempting to upload any data in the database,
     * using the given frequency.
     *
     * @param c         Context within which to peform the operation
     * @param frequency How often to poll for internet connectivity, in minutes
     */
    public static void startUpdateServiceAtFrequency(Context c, int frequency)
    {
        Intent updateIntent = new Intent(c, UploadReceiver.class);
        PendingIntent pendingUpdateIntent = PendingIntent
                .getBroadcast(c,
                              PENDINGINTENT_UPLOAD_ID,
                              updateIntent,
                              PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        Calendar time = Calendar.getInstance();

        // Creates an alarm that fires immediately, and then repeating at set intervals.
        manager.setRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                             frequency * 60 * 1000,
                             pendingUpdateIntent);
        Log.d("[Upload]", "Upload service started.");
    }

    /**
     * Starts the alarm service for periodically attempting to poll for current GPS location.
     *
     * @param c Context within which to perform the operation.
     */
    public static void startTrackingService(Context c)
    {
        startTrackingServiceAtFrequency(c, getIntPref(c, "tracking_freq"));
    }

    /**
     * Starts the alarm service for periodically attempting to poll for current GPS location at
     * the given frequency.
     *
     * @param c         Context within which to perform the operation
     * @param frequency How often to poll for GPS, in minutes
     */
    public static void startTrackingServiceAtFrequency(Context c, int frequency)
    {
        if (getBoolPref(c, "enable_tracking"))
        {
            startTrackingServiceIgnoreToggle(c, frequency);
        }
        else
        {
            Log.d("[Tracking]", "Attempted to start tracking, but it is currently disabled");
        }
    }

    /**
     * Starts the alarm service for periodically attempting to poll for current GPS location
     * without testing if tracking is enabled.
     *
     * @param c         Context within which to perform the operation
     * @param frequency How often to poll for GPS, in minutes
     */
    public static void startTrackingServiceIgnoreToggle(Context c, int frequency)
    {
        Intent intent = new Intent(c, TrackingReceiver.class);
        PendingIntent pendingIntent = PendingIntent
                .getBroadcast(c,
                              PENDINGINTENT_TRACK_ID,
                              intent,
                              PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager manager = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
        Calendar time = Calendar.getInstance();

        // Creates an alarm that repeats at set intervals.
        time.add(Calendar.MINUTE, frequency);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                             frequency * 60 * 1000,
                             pendingIntent);
        Log.d("[Tracking]", "Tracking service started.");
    }

    /**
     * Stops the tracking service.
     *
     * @param c Context within which to perform the operation.
     */
    public static void stopTrackingService(Context c)
    {
        Intent intent = new Intent(c, TrackingReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(c, PENDINGINTENT_TRACK_ID, intent,
                                                          PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) c.getSystemService(ALARM_SERVICE);

        if (sender != null)
        {
            alarmManager.cancel(sender);
            Log.d("[Tracking]", "Tracking service CANCELLED.");
        }
        else
        {
            Log.d("[Tracking]", "Attempted to cancel tracking, but could not identify service.");
        }
    }

    /**
     * Starts the service responsible for periodically checking if any images should be deleted,
     * and deletes them.
     *
     * @param c Context within which to perform the operation
     */
    public static void startPictureCleanupService(Context c)
    {
        Intent intent = new Intent(c, PictureCleanupReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(c, PENDINGINTENT_CLEANUP_ID, intent,
                                                                 PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) c.getSystemService(ALARM_SERVICE);
        Calendar time = Calendar.getInstance();

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(),
                                  PICTURE_CLEANUP_FREQ * 60 * 60 * 1000,
                                  pendingIntent);
        Log.d("[Image Cleanup]", "Image cleanup service started.");
    }

    /**
     * Identifies and returns the appropriate file storage location for pictures.
     *
     * @param c Context to use as basis for finding file directory
     * @return File object referring to the picture directory
     */
    public static File getDir(Context c)
    {
        File dir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).getAbsolutePath());
        return new File(dir, "GPSSnapshot");
    }

    /**
     * Adds values into SQL database.
     *
     * @param c           Context within which to perform the operation
     * @param picPath     Path of the saved picture
     * @param time        Time the picture was taken,
     *                    as a string formatted by GPSSnapshot.DATE_FORMAT
     * @param description Description associated with the data
     * @param groupName   Name of the group to associate the data with
     * @param isUrgent    Whether the data entry is urgent or not
     * @param location    All location information associated with the picture
     */
    public static void addDataEntry(Context c, String picPath, String time,
                                    String description, String groupName, boolean isUrgent,
                                    Location location)
    {
        // Initialize database access
        DatabaseHandler mDbHelper = new DatabaseHandler(c);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        ContentValues vals = new ContentValues();

        String reportString;

        vals.put(DatabaseHandler.PIC_PATH, picPath);
        vals.put(DatabaseHandler.TIME, time);
        vals.put(DatabaseHandler.DESCRIPTION, description);
        vals.put(DatabaseHandler.GROUP_NAME, groupName);
        vals.put(DatabaseHandler.IS_URGENT, isUrgent);
        vals.put(DatabaseHandler.SUBMITTED, 0);

        try
        {
            Date date = GPSSnapshot.DATE_FORMAT.parse(time);


            // Location can be null if GPS could not resolve a location.  If not null, format and store
            //  the location data
            if (location != null)
            {
                double lati = location.getLatitude();
                double longi = location.getLongitude();

                vals.put(DatabaseHandler.LATITUDE, lati);
                vals.put(DatabaseHandler.LONGITUDE, longi);

                reportString = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                        .format(date.getTime())) + " - [" + Double.toString(lati) + "][" +
                               Double.toString(longi) + "]";
            }
            else  // If location was null, submit without GPS data
            {
                reportString = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                        .format(date.getTime())) + " - [NO GPS DATA]";
            }

            // Insert the data into the database
            mDb.replace(DatabaseHandler.TABLE_NAME, null, vals);


            Log.d("[Data]", "Data saved. (" + reportString + ")");

        } catch (ParseException e) { e.printStackTrace(); }

        mDb.close();

        // Try to force immediate upload of photo
        GPSSnapshot.forceDataUpload(c);
    }

    /** Triggers the uploading service immediately rather than waiting for the next scheduled
     * upload time.
     *
     * @param c Context within which to perform the operation.
     */
    public static void forceDataUpload(Context c)
    {
        Log.d("[Upload]", "Forcing data upload by broadcast...");
        c.sendBroadcast(new Intent(UploadReceiver.ACTION_FORCE_UPLOAD));
    }

    /**
     * Goes through the local database to see if any pictures are due to be cleaned up, and deletes
     * them.
     *
     * @param c Context within which to perform the operation.
     */
    public static void cleanPicturesFolder(Context c)
    {
        // Calculate how many days the images should be kept, based on preferences
        int daysToKeep = retentionAsInt(c, getStringPref(c, "image_retention"));
        // If the method returns -1, this means that images should be kept indefinitely.
        // Cancel the cleaning of the pictures folder.
        if (daysToKeep == -1)
        {
            Log.d("[Image Cleanup]", "Images are to be kept indefinitely.  Aborting cleanup.");
            return;
        }

        Log.d("[Image Cleanup]", "Attempting to clean images folder...");

        // Initialize database access
        DatabaseHandler mDbHelper = new DatabaseHandler(c);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        // Retrieve all database elements that have already been submitted
        Cursor dataCursor = mDb.query(DatabaseHandler.TABLE_NAME, DatabaseHandler.COLUMNS,
                                      DatabaseHandler.SUBMITTED + " = '1'",
                                      null, null, null, null);

        if (dataCursor.getCount() == 0)
            Log.d("[Image Cleanup]", "No images require cleaning.");

        // Go through each submitted data entry and determine if the image is past its expiration
        while (dataCursor.moveToNext())
        {
            try
            {
                Calendar deleteTime = Calendar.getInstance();
                Calendar currentTime = Calendar.getInstance();
                String timeString = dataCursor.getString(
                        dataCursor.getColumnIndex(DatabaseHandler.TIME));

                deleteTime.setTime(DATE_FORMAT.parse(timeString));
                deleteTime.add(Calendar.DATE, daysToKeep);

                // If the current time has already passed the deletion time
                if (deleteTime.compareTo(currentTime) < 0)
                {
                    // Delete the image
                    int success = deleteImage(c, dataCursor.getString(
                            dataCursor.getColumnIndex(DatabaseHandler.PIC_PATH)));

                    // Delete entry in database
                    if (success == IMAGE_DELETION_SUCCESSFUL ||
                        success == IMAGE_DELETION_NOTFOUND)
                    {
                        mDb.delete(DatabaseHandler.TABLE_NAME,
                                   DatabaseHandler.TIME + " = " + timeString,
                                   null);
                    }
                }


            } catch (ParseException e)
            {
                e.printStackTrace();
            }
        }

        dataCursor.close();
        mDb.close();

    }

    /**
     * Attempts to delete the image at the given file location, and returns a value representing
     * its success.
     *
     * @param c        Context within which to perform the operation
     * @param filename File name of image to delete.
     * @return Success code of operation.  // TODO: Explain this
     */
    public static int deleteImage(Context c, String filename)
    {
        // Find image to delete
        File file = new File(filename);

        if (!file.exists())  // If file could not be found
        {
            Log.d("[Image Cleanup]", "Could not find image to be deleted: " + filename);
            return IMAGE_DELETION_NOTFOUND;

        }
        else if (!file.delete())
        {  // Attempt to delete file here and check result
            Log.d("[Image Cleanup]", "Unable to delete image: " + filename);
            return IMAGE_DELETION_FAILED;
        }
        else
        {
            Log.d("[Image Cleanup]", "Successfully deleted image: " + filename);
            return IMAGE_DELETION_SUCCESSFUL;
        }
    }

    /**
     * Returns the integer value (in days) that images should be stored before being deleted,
     * represented by the given string formatted by the settings.
     *
     * @param c         Context within which to perform the operation.
     * @param retention String to convert to an integer value
     * @return The number of days represented by the given string, or -1 if the string represents
     * that images should not be deleted.
     */
    public static int retentionAsInt(Context c, String retention)
    {
        if (retention.equals(c.getString(R.string.pref_forever)))
        {
            return -1;
        }
        else if (retention.equals(c.getString(R.string.pref_until_upload)))
        {
            return 0;
        }
        else if (retention.equals(c.getString(R.string.pref_one_day)))
        {
            return 1;
        }
        else if (retention.equals(c.getString(R.string.pref_one_week)))
        {
            return 7;
        }
        else if (retention.equals(c.getString(R.string.pref_two_weeks)))
        {
            return 14;
        }
        else if (retention.equals(c.getString(R.string.pref_one_month)))
        {
            return 30;
        }
        else if (retention.equals(c.getString(R.string.pref_three_months)))
        {
            return 90;
        }
        else // Something went wrong.  Default to not deleting images.
            return -1;

    }

    /**
     * Retrieves the current group from the stored preferences, and returns it.
     *
     * @param c Context within which to perform the operation
     * @return The current group
     */
    public static DataGroup getCurrentGroup(Context c)
    {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);

        String currGroupName = settings.getString("currgroup_name", "");
        String currGroupAccessCode = settings.getString("currgroup_access_code", "");
        String currGroupDescription = settings.getString("currgroup_description", "");
        String currGroupAppId = settings.getString("currgroup_app_id", "");
        String currGroupClientKey = settings.getString("currgroup_client_key", "");
        boolean currGroupIsAdmin = settings.getBoolean("currgroup_is_admin", false);
        boolean currGroupIsPublicEntries = settings.getBoolean("currgroup_is_public", false);

        if (currGroupName.equals(""))
            return null;

        DataGroup currentGroup = new DataGroup();
        currentGroup.title = currGroupName;
        currentGroup.description = currGroupDescription;
        currentGroup.accessCode = currGroupAccessCode;
        currentGroup.appId = currGroupAppId;
        currentGroup.clientKey = currGroupClientKey;
        currentGroup.isAdmin = currGroupIsAdmin;
        currentGroup.publicEntries = currGroupIsPublicEntries;

        Log.d("[Groups]", "Returning current group: " + currentGroup.toString());

        return currentGroup;
    }

    /**
     * Returns a set of all groups in My Groups.
     *
     * @param c Context within which to peform the operation.
     * @return Array List containing all groups.
     */
    public static Set<DataGroup> getMyGroups(Context c)
    {
        DatabaseHandler mDbHelper = new DatabaseHandler(c);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        // Query all groups stored locally
        Cursor groupCursor = mDb.query(DatabaseHandler.GROUP_TABLE_NAME,
                                       DatabaseHandler.GROUP_COLUMNS, null, null, null, null,
                                       null);

        Set<DataGroup> returnList = new LinkedHashSet<>();
        // Iterate through the cursor and store each data group in the array
        while (groupCursor.moveToNext())
        {
            DataGroup newGroup = DataGroup.fromSQLEntries(groupCursor);

            returnList.add(newGroup);

            Log.d("[Groups]", "Found group: " + newGroup.toString());
        }

        groupCursor.close();
        mDb.close();

        return returnList;
    }

    /**
     * Stores the given data group as the current group in the stored settings.
     *
     * @param c     Context within which to perform the operation
     * @param group The data group to set as current
     */
    public static void setCurrentGroup(Context c, DataGroup group)
    {
        Log.d("[Groups]", "Setting current group: " + group.toString());

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString("currgroup_name", group.title);
        editor.putString("currgroup_description", group.description);
        editor.putString("currgroup_access_code", group.accessCode);
        editor.putString("currgroup_app_id", group.appId);
        editor.putString("currgroup_client_key", group.clientKey);
        editor.putBoolean("currgroup_is_admin", group.isAdmin);
        editor.putBoolean("currgroup_is_public", group.publicEntries);

        // Commit in the background, NOT in-line
        editor.apply();
    }

    /**
     * Tests whether the given group is in My Groups.
     *
     * @param c     Context within which to perform the operation.
     * @param group The group to test
     * @return Returns true if the given group is in My Groups.  False otherwise.
     */
    public static boolean isInMyGroups(Context c, DataGroup group)
    {
        DatabaseHandler mDbHelper = new DatabaseHandler(c);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        Cursor cursor = mDb.query(DatabaseHandler.GROUP_TABLE_NAME, DatabaseHandler.GROUP_COLUMNS,
                                  DatabaseHandler.GROUP_TITLE + " = '" + group.title + "'",
                                  null, null, null, null);

        boolean result = cursor.getCount() > 0;
        mDb.close();
        cursor.close();

        return (result);
    }

    /**
     * Returns the data group associated with the given group title
     *
     * @param c         Context within which to perform the operation.
     * @param groupName Name of the group to find
     * @return
     */
    public static DataGroup getGroupByName(Context c, String groupName)
    {
        DatabaseHandler mDbHelper = new DatabaseHandler(c);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        Cursor cursor = mDb.query(DatabaseHandler.GROUP_TABLE_NAME, DatabaseHandler.GROUP_COLUMNS,
                                  DatabaseHandler.GROUP_TITLE + " = '" + groupName + "'",
                                  null, null, null, null);
        if (cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            return DataGroup.fromSQLEntries(cursor);
        }
        else
            return null;


    }

    /**
     * Add the given group the local My Groups SQL database.
     *
     * @param c     Context within which to perform the operation.
     * @param group Data group to store
     */
    public static void addGroup(Context c, DataGroup group)
    {

        DatabaseHandler mDbHelper = new DatabaseHandler(c);
        SQLiteDatabase mDb = mDbHelper.getWritableDatabase();

        ContentValues vals = new ContentValues();
        vals.put(DatabaseHandler.GROUP_TITLE, group.title);
        vals.put(DatabaseHandler.GROUP_DESCRIPTION, group.description);
        vals.put(DatabaseHandler.GROUP_ACCESS_CODE, group.accessCode);
        vals.put(DatabaseHandler.GROUP_DB_APPID, group.appId);
        vals.put(DatabaseHandler.GROUP_DB_CLIENTKEY, group.clientKey);
        vals.put(DatabaseHandler.GROUP_IS_ADMIN, (group.isAdmin ? 1 : 0));
        vals.put(DatabaseHandler.GROUP_IS_PUBLIC_ENTRIES, (group.publicEntries ? 1 : 0));

        mDb.replace(DatabaseHandler.GROUP_TABLE_NAME, null, vals);

        mDb.close();

        Log.d("[Groups]", "Added group: " + group.toString());
    }

    /**
     * Logs the user into the Parse database, and performing the given tasks upon success and
     * failure.  There are three successful outcomes:
     * 1.  If the user has not established username and password credentials, log in as an
     * anonymous user.
     * 2.  Log in to an existing account using the given credentials
     * 3.  Attempt to log in using the given credentials, but if the user has not yet been created,
     * register the user with these credentials.
     * <p/>
     * If either of these 3 successful outcomes occurs, {@link LogInResponse#onSuccess()} is called
     * on the given {@link LogInResponse} object.
     * <p/>
     * If the password does not match the e-mail given, then {@link LogInResponse#onInvalid()} is
     * called on the given {@link LogInResponse} object.
     * <p/>
     * If there is any other kind of failure, {@link LogInResponse#onFailure(int)} is called on the
     * given {@link LogInResponse} object, passing along the error code associated with the failure.
     *
     * @param c        Context within which to peform the operation
     * @param response Callback to handle success, failure, and invalid logins.
     */
    public static void logInUser(final Context c, final LogInResponse response)
    {

        Log.d("[Parse]", "Starting Parse connection...");

        // Enable anonymous users
        ParseUser.enableAutomaticUser();

        // If there currently exists a user name to attempt to log in as
        if (!GPSSnapshot.getStringPref(c, "user_email").equals("") &&
            ParseUser.getCurrentUser().get("username") == null)
        {

            Log.d("[Parse]", "Attempting to log in as " + GPSSnapshot
                    .getStringPref(c, "user_email"));

            // Attempt to log in
            ParseUser.logInInBackground(
                    GPSSnapshot.getStringPref(c, "user_email"),
                    GPSSnapshot.getStringPref(c, "user_password"),
                    new LogInCallback()
                    {
                        @Override
                        public void done(ParseUser parseUser, com.parse.ParseException e)
                        {
                            if (e != null)
                            {
                                Log.e("[Parse]", "Login failed.\n" + e.getMessage()
                                                 + "\nError Code = " + e.getCode());
                                e.printStackTrace();
                                // If the given user name does not exist, attempt to sign it up.
                                if (e.getCode() == com.parse.ParseException.OBJECT_NOT_FOUND ||
                                    e.getCode() == com.parse.ParseException
                                            .MUST_CREATE_USER_THROUGH_SIGNUP)
                                {
                                    Log.d("[Parse]", "Attempting to register user...");
                                    signUpUser(c, response);
                                }
                                // If the password is incorrect, respond that the log in was invalid
                                else if (e.getCode() == com.parse.ParseException
                                        .INVALID_EMAIL_ADDRESS ||
                                         e.getCode() == com.parse.ParseException.PASSWORD_MISSING ||
                                         e.getCode() == com.parse.ParseException.USERNAME_MISSING)
                                {
                                    if (response != null)
                                        response.onInvalid();
                                }
                                // If some other error occurs, respond with failure
                                else
                                {
                                    Log.e("[Parse]", "Unrecoverable error.");
                                    if (response != null)
                                        response.onFailure(e.getCode());
                                }
                            }
                            else // If the user logged in, respond with success
                            {
                                Log.d("[Parse]", "Login successful.");
                                if (response != null)
                                    response.onSuccess();

                                ParseUser currentUser = ParseUser.getCurrentUser();
                                currentUser.put("phone_number", GPSSnapshot.getStringPref(c, "user_phone"));
                                currentUser.put("display_name", GPSSnapshot.getStringPref(c, "display_name"));
                                currentUser.saveEventually(null);

                                // Update the current user of this installation for push notifications
                                ParseInstallation.getCurrentInstallation()
                                                 .put("currentuser", currentUser);
                                ParseInstallation.getCurrentInstallation().saveInBackground(null);
                            }

                        }
                    });
        }
        else // If there is no username, implicit anonymous user was created, respond with success.
        {
            Log.d("[Parse]", "Created anonymous user to use.");
            if (response != null)
                response.onSuccess();
        }

    }

    /**
     * Creates a new Parse User with the stored user credentials.
     *
     * @param c        Context within which to perform the operation
     * @param response Callback to handle success, failure, and invalid logins.
     */
    public static void signUpUser(final Context c, final LogInResponse response)
    {
        ParseUser.enableAutomaticUser();
        ParseUser user = ParseUser.getCurrentUser();

        user.put("username", GPSSnapshot.getStringPref(c, "user_email"));
        user.put("password", GPSSnapshot.getStringPref(c, "user_password"));
        user.put("email", GPSSnapshot.getStringPref(c, "user_email"));
        user.put("phone_number", GPSSnapshot.getStringPref(c, "user_phone"));

        user.signUpInBackground(new SignUpCallback()
        {
            @Override
            public void done(com.parse.ParseException e)
            {
                if (e != null)
                {
                    Log.e("[Parse]", "Registering anonymous user failed.\n" + e.getMessage()
                                     + "\nError Code = " + e.getCode());
                    e.printStackTrace();
                    if (response != null)
                        response.onFailure(e.getCode());
                    ParseUser.logOutInBackground(null);
                }
                else
                {
                    Log.d("[Parse]", "Successfully registered anonymous user with e-mail " +
                                     GPSSnapshot.getStringPref(c, "user_email"));
                    if (response != null)
                        response.onSuccess();
                    // Update the current user of this installation for push notifications
                    ParseInstallation.getCurrentInstallation()
                                     .put("currentuser",
                                          ParseUser.getCurrentUser());
                    ParseInstallation.getCurrentInstallation().saveEventually(null);
                }
            }
        });
    }

    /**
     * Logs out the current user.
     *
     * @param c        Context within which to perform the operation.
     * @param response Callback to handle success and failure
     */
    public static void logOutUser(final Context c, final LogInResponse response)
    {
        if (ParseUser.getCurrentUser() != null)
        {
            Log.d("[Parse]", "Logging out current user...");
            ParseUser.getCurrentUser().logOutInBackground(new LogOutCallback()
            {
                @Override
                public void done(com.parse.ParseException e)
                {
                    if (response == null)
                        return;

                    if (e != null)
                        response.onFailure(e.getCode());
                    else
                        response.onSuccess();
                }
            });
        }
    }

    /**
     * Test if the device is currently connected to the internet in some fashion.
     *
     * @param c Context within which to perform the operation.
     * @return Returns true if the device is connected to the internet.  False otherwise.
     */
    public static boolean isInternetConnected(Context c)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                c.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Tests if the given string matches the following regex:
     * "\\p{Alpha}[\\p{Alnum}_ ]{1,14}\\p{Alnum}"
     * Or put more simply:
     * One letter character, followed by 1 to 14 characters that are either letters, numbers,
     * spaces, or underscores, and ends with one character that is either a letter or a number.
     *
     * @param name String to test
     * @return Returns true if the string matches the regex.  False otherwise.
     */
    public static boolean isValidName(String name)
    {
        return name.matches("\\p{Alpha}[\\p{Alnum}_ ]{1,14}\\p{Alnum}");
    }


    /**
     * Generic method for displaying an error dialog.
     *
     * @param c              Context within which to perform the operation.
     * @param errorCode      Constant integer value representing a given error.
     * @param buttonListener Optional callback when dialog is closed.  Can be left null.
     */
    public static void generateErrorDialog(Context c, int errorCode,
                                           DialogInterface.OnClickListener buttonListener)
    {
        if (buttonListener == null)
        {
            buttonListener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    // DO NOTHING
                }
            };
        }

        String errorTitle, errorMessage;
        switch (errorCode)
        {
            case ERROR_NO_INTERNET:
                errorTitle = c.getString(R.string.E_TITLE_No_Internet);
                errorMessage = c.getString(R.string.E_TEXT_No_Internet);
                break;
            case ERROR_BAD_LOGIN:
                errorTitle = c.getString(R.string.E_TITLE_Login);
                errorMessage = c.getString(R.string.E_TEXT_Bad_Login);
                break;
            case ERROR_NO_LOGIN:
                errorTitle = c.getString(R.string.E_TITLE_Login);
                errorMessage = c.getString(R.string.E_TEXT_No_Login);
                break;
            case ERROR_INVALID_LOGIN:
                errorTitle = c.getString(R.string.E_TITLE_Login);
                errorMessage = c.getString(R.string.E_TEXT_Invalid_Login);
                break;
            case ERROR_UNAUTHENTICATED_LOGIN:
                errorTitle = c.getString(R.string.E_TITLE_Login);
                errorMessage = c.getString(R.string.E_TEXT_Unauthenticated_Login);
                break;
            case ERROR_PARSE:
                errorTitle = c.getString(R.string.E_TITLE_Parse);
                errorMessage = c.getString(R.string.E_TEXT_Parse);
                break;
            case ERROR_INVALID_NAME:
                errorTitle = c.getString(R.string.E_TITLE_Name);
                errorMessage = c.getString(R.string.E_TEXT_Invalid_Name);
                break;
            case ERROR_TAKEN_NAME:
                errorTitle = c.getString(R.string.E_TITLE_Name);
                errorMessage = c.getString(R.string.E_TEXT_Taken_Name);
                break;
            case ERROR_RESERVED_NAME:
                errorTitle = c.getString(R.string.E_TITLE_Name);
                errorMessage = c.getString(R.string.E_TEXT_Reserved_Name);
                break;
            case ERROR_TEMPORARY_USER_NEW_GROUP:
                errorTitle = c.getString(R.string.E_TITLE_User);
                errorMessage = c.getString(R.string.E_TEXT_Temporary_User_New_Group);
                break;
            case ERROR_UNKNOWN:
                errorTitle = c.getString(R.string.E_TITLE_Unknown);
                errorMessage = c.getString(R.string.E_TEXT_Unknown);
                break;
            default:
                errorTitle = "";
                errorMessage = "";
        }

        Log.e("[ERROR]", errorTitle + ":  " + errorMessage);

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle(errorTitle)
               .setMessage(errorMessage)
               .setPositiveButton(R.string.E_CONFIRM, buttonListener);
        builder.show();


    }

    /**
     * Creates an error notification for when logging in to Parse fails.
     *
     * @param c Context within which to perform the operation
     */
    public static void generateLoginErrorNotification(Context c)
    {
        PendingIntent pendingIntent = PendingIntent.getActivity(
                c,
                0,
                new Intent(c, SettingsActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new android.support.v7.app.
                NotificationCompat.Builder(c);
        Bitmap bm = BitmapFactory.decodeResource(c.getResources(), R.drawable.logo2);
        builder.setSmallIcon(R.drawable.logo2)
               .setLargeIcon(bm)
               .setContentTitle(c.getString(R.string.error_login_notif_title))
               .setContentText(c.getString(R.string.error_login_notif_description))
               .setContentIntent(pendingIntent);

        ((NotificationManager) c.getSystemService(Context.NOTIFICATION_SERVICE))
                .notify(1, builder.build());
    }
}
