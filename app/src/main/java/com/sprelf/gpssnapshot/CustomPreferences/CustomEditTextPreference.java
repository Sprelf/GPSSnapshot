package com.sprelf.gpssnapshot.CustomPreferences;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;

import com.sprelf.gpssnapshot.Activities.SettingsActivity;

/**
 * Created by Chris on 28.10.2015.
 */
public class CustomEditTextPreference extends EditTextPreference
{
    private View view;
    private CustomPreference.CustomPrefBindingListener listener;

    public CustomEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToActivity()
    {
        listener = (SettingsActivity)getContext();
    }


    @Override
    public void onBindView(View view)
    {
        super.onBindView(view);
        this.view = view;

        listener.onBindView(view, getKey());
    }

    public View getView()
    {
        return this.view;
    }



}
