package com.sprelf.gpssnapshot.CustomPreferences;

import android.content.Context;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;

import com.sprelf.gpssnapshot.Activities.SettingsActivity;

/**
 * Created by Chris on 28.10.2015.
 */
public class CustomPreference extends Preference
{
    private View view;
    private CustomPrefBindingListener listener;

    public CustomPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onAttachedToActivity()
    {
        listener = (SettingsActivity)getContext();
    }


    @Override
    public void onBindView(View view)
    {
        super.onBindView(view);
        this.view = view;

        listener.onBindView(view, getKey());

    }

    public View getView()
    {
        return this.view;
    }

    public interface CustomPrefBindingListener
    {
        public void onBindView(View view, String key);
    }

}
