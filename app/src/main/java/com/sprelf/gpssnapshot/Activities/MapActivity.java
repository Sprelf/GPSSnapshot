package com.sprelf.gpssnapshot.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sprelf.gpssnapshot.Adapters.DataWindowAdapter;
import com.sprelf.gpssnapshot.Adapters.UserFilterListAdapter;
import com.sprelf.gpssnapshot.CustomViews.MapWrapperLayout;
import com.sprelf.gpssnapshot.Fragments.FilterFragment;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.Receivers.UploadReceiver;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;
import com.sprelf.gpssnapshot.UtilityClasses.GroupResults;
import com.sprelf.gpssnapshot.UtilityClasses.ImageHandler;
import com.sprelf.gpssnapshot.UtilityClasses.ImageResponse;
import com.sprelf.gpssnapshot.UtilityClasses.LogInResponse;
import com.sprelf.gpssnapshot.UtilityClasses.SlidingAnimation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

public class MapActivity extends Activity implements OnMapReadyCallback,
                                                     FilterFragment.OnFilterChangeListener
{
    private ParseUser user;
    private MapFragment mapFragment;
    private GoogleMap map;
    private ArrayList<ParseObject> objects;
    private ArrayList<Marker> markers;
    private ArrayList<ImageHandler> images;
    private boolean filterOptionsOpen = false;
    private String currentGroupName;

    private String intentZoomId;
    private boolean zoomed = false;

    private Calendar filterStartDate, filterEndDate;
    private boolean filterUrgentOnly;
    private LinkedHashMap<String, Boolean> filterNames;
    private LinkedHashMap<String, GroupResults> groups;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_map);

        // Properly set up action bar
        ActionBar actionBar = getActionBar();
        actionBar.setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (getIntent()!=null && getIntent().getData()!=null)
        {
            Uri data = getIntent().getData();
            String[] dataComponents = data.getPath().replace("/", "").split("&");
            currentGroupName = dataComponents[0];
            intentZoomId = dataComponents[1];
            Log.d("[Map]", "Received intent, searching for object ID '"+intentZoomId+
                 "' in group '"+currentGroupName+"'");
        }
        else
        {
            Log.d("[Map]", "No intent received, starting normally.");
            intentZoomId = null;
            // Get current group name
            DataGroup currentGroup = GPSSnapshot.getGroupByName(this, currentGroupName);
            if (currentGroup!=null)
                currentGroupName = currentGroup.title;
        }

        // Initialize arraylists
        markers = new ArrayList<>();
        images = new ArrayList<>();
        objects = new ArrayList<>();
        groups = new LinkedHashMap<>();



    }

    @Override
    public void onStart()
    {
        super.onStart();

        // Start loading map
        mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.MapViewer_MapFragment);
        mapFragment.getMapAsync(this);
    }

    /**
     * Handles the pressing of the back button.  Closes filter options fragment if it is open,
     * otherwise performs default behavior
     */
    @Override
    public void onBackPressed()
    {
        if (filterOptionsOpen)
            closeFilterMenu();
        else
            super.onBackPressed();
    }

    /**
     * Called once map has finished loading.
     *
     * @param googleMap The loaded map
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        Log.d("[Map]", "Map successfully loaded and ready.");

        user = logInUser();


        // Set up map
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        MapWrapperLayout mapWrapper = (MapWrapperLayout)findViewById(R.id.MapViewer_MapWrapper);
        mapWrapper.init(map, getResources().getDimensionPixelSize(R.dimen.MapViewer_InfoWindowOffset));

        map.setInfoWindowAdapter(new DataWindowAdapter(this, markers, objects, images, mapWrapper));
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker)
            {
                zoomToMarker(marker);

                return true; // Consume the event since it was dealt with
            }
        });




        // Test for first time use to general tutorial
        testForFirstTimeUse();
    }

    private void zoomToMarker(Marker marker)
    {
        if (marker==null)
            return;

        // Calculate required vertical shift for current screen density
        final int dY = (int) (getResources().getDimensionPixelSize(
                R.dimen.MapViewer_InfoWindowHeight) * (2f / 3f));
        final Projection projection = map.getProjection();
        final Point markerPoint = projection.toScreenLocation(
                marker.getPosition());
        // Shift the point we will use to center the map
        markerPoint.offset(0, -dY);
        final LatLng newLatLng = projection.fromScreenLocation(markerPoint);

        map.animateCamera(CameraUpdateFactory.newLatLng(newLatLng));
        // Show the info window (as the overloaded method would)
        marker.showInfoWindow();
    }

    @Override
    public void onLowMemory()
    {
        // When the system is running low on storage, cache all previously downloaded images.
        Log.d("[Image Storage]", "Low on memory.  Starting image storage...");
        for (LinkedHashMap.Entry<String, GroupResults> entry : groups.entrySet())
        {
            if (!entry.getKey().equals(currentGroupName))
            {
                Log.d("[Image Storage]", "Storing " + entry.getValue().images.size() +
                                         " images in group " + entry.getKey());
                for (ImageHandler image : entry.getValue().images)
                {
                    image.storeImage();
                }
            }
        }
        super.onLowMemory();
    }

    @Override
    public void onDestroy()
    {
        for (LinkedHashMap.Entry<String, GroupResults> entry : groups.entrySet())
        {
            for (ImageHandler image : entry.getValue().images)
            {
                image.destroy();
            }
        }
        super.onDestroy();
    }

    /**
     * On Click method for the filter toggle button.  Handles opening and closing the filter
     * options window.
     *
     * @param v View that was clicked
     */
    public void onFilterToggleClick(View v)
    {
        if (filterOptionsOpen)
        {
            closeFilterMenu();
        }
        else
        {
            openFilterMenu();

        }
    }

    /**
     * Performs all actions necessary to open up the filter options fragment
     */
    private void openFilterMenu()
    {
        // Change the filter toggle icon
        ((ImageView) findViewById(R.id.MapViewer_FilterToggle))
                .setImageResource(R.drawable.ic_ab_back_holo_dark);
        // Mark filter options as being open
        filterOptionsOpen = true;

        //
        View fragView = findViewById(R.id.MapViewer_FilterFragment);
        // Set the width of the fragment to its appropriate value
        fragView.getLayoutParams().width = getResources().getDimensionPixelSize(
                R.dimen.MapViewer_FilterFragmentWidth);
        // Shift the fragment down slightly to remove bottom border
        fragView.setY(fragView.getY() + 2);
        // Call for a view update
        fragView.requestLayout();
        // Start animation to slide fragment in from the left
        SlidingAnimation anim = new SlidingAnimation(fragView);
        anim.setDuration(500);
        anim.setParams(-getResources().getDimensionPixelSize(
                R.dimen.MapViewer_FilterFragmentWidth), -2);
        fragView.startAnimation(anim);
    }

    /**
     * Performs all actions necessary to close the filter options fragment.
     */
    private void closeFilterMenu()
    {
        // Change the filter toggle icon
        ((ImageView) findViewById(R.id.MapViewer_FilterToggle))
                .setImageResource(R.drawable.ic_menu_moreoverflow_normal_holo_dark);
        // Mark filter options as being closed
        filterOptionsOpen = false;

        // Shrink the filter options menu to a width of 0 instantly.
        View fragView = findViewById(R.id.MapViewer_FilterFragment);
        fragView.getLayoutParams().width = 0;
        fragView.requestLayout();

    }

    /**
     * Handles logging the user into Parse.  If successful, calls appropriate Parse data queries.
     *
     * @return Reference to the current user after logging in.
     */
    private ParseUser logInUser()
    {
        GPSSnapshot.logInUser(this, new LogInResponse()
        {
            @Override
            public void onSuccess()
            {
                user = ParseUser.getCurrentUser();
                if (currentGroupName==null || currentGroupName.equals(""))
                {
                    makeParseQueryWithUser();
                }
                else
                {
                    //makeParseQueryWithGroup(currentGroupName);
                    // Let this be performed by Spinner's On Item Selected method
                }
            }

            @Override
            public void onInvalid()
            {
                GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                GPSSnapshot.ERROR_INVALID_LOGIN, null);
            }

            @Override
            public void onFailure(int errorCode)
            {
                GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                GPSSnapshot.ERROR_BAD_LOGIN, null);
            }
        });

        return ParseUser.getCurrentUser();
    }


    /**
     * Queries Parse for all data submitted by the current user.
     */
    private void makeParseQueryWithUser()
    {
        if (!GPSSnapshot.isInternetConnected(this))
        {
            GPSSnapshot.generateErrorDialog(this, GPSSnapshot.ERROR_NO_INTERNET, null);
            return;
        }

        Log.d("[Map]", "Querying Parse for all images from this user...");

        // Set current filter text box to indicate that it is querying for user data
        TextView currFilterText = (TextView) findViewById(R.id.MapViewer_CurrentFilterText);
        currFilterText.setText(R.string.MapViewer_CurrentUserLabel);

        // Construct query and launch it
        ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("_User");
        innerQuery.whereEqualTo("objectId", user.getObjectId());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("DataPoint");
        query.whereMatchesQuery(UploadReceiver.USER_KEY, innerQuery);
        launchQuery(query);
    }

    /**
     * Queries Parse for all data belonging to the given group.
     *
     * @param groupName Group to query for.
     */
    private void makeParseQueryWithGroup(String groupName)
    {
        if (!GPSSnapshot.isInternetConnected(this))
        {
            GPSSnapshot.generateErrorDialog(this, GPSSnapshot.ERROR_NO_INTERNET, null);
            return;
        }

        // Set current filter text box to reflect filtering by the given group
        TextView currFilterText = (TextView) findViewById(R.id.MapViewer_CurrentFilterText);
        TextView currGroupText = (TextView) findViewById(R.id.MapViewer_CurrentGroupText);
        currFilterText.setText(R.string.MapViewer_CurrentGroupLabel);
        currGroupText.setText(groupName);


        // Test if the group has already been loaded into memory before.  If so, load it instead
        //  of querying for it again.
        if (groups.containsKey(groupName))
        {
            Log.d("[Map]",
                  "Recalling previously queried images in the group '" + groupName + "'.");
            // Pulling the data and putting in arrays
            objects.clear();
            objects.addAll(groups.get(groupName).objects);
            images.clear();
            images.addAll(groups.get(groupName).images);

            // Set up map
            setInitialFilter();
            clearMarkers();
            for (ParseObject obj : objects)
                addMarker(obj);
            applyFilter();
        }
        else
        {
            // Construct query and launch it
            Log.d("[Map]", "Querying Parse for all images in the group '" + groupName + "'.");
            ParseQuery<ParseObject> query = ParseQuery.getQuery("DataPoint");
            query.whereEqualTo(UploadReceiver.GROUP_NAME_KEY, groupName);

            DataGroup currentGroup = GPSSnapshot.getGroupByName(this, currentGroupName);
            if (currentGroup!=null && !currentGroup.publicEntries && !currentGroup.isAdmin)
                query.whereEqualTo(UploadReceiver.USER_KEY, ParseUser.getCurrentUser());

            launchQuery(query);
        }
    }

    /**
     * Performs the given Parse query for data and handles the results.
     *
     * @param query Parse Query to perform.
     */
    private void launchQuery(ParseQuery<ParseObject> query)
    {
        // Launch the query
        query.findInBackground(new FindCallback<ParseObject>()
        {
            @Override
            public void done(List<ParseObject> list, ParseException e)
            {
                if (e != null)
                {
                    Log.e("[Map]", "Error with Parse query.\n" + e.getMessage() +
                                   "\nError Code = " + e.getCode());
                    e.printStackTrace();
                    GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                    GPSSnapshot.ERROR_PARSE, null);
                }
                else
                {
                    // Reset the objects list with the query results without dereferencing current
                    //  objects array.
                    objects.clear();
                    objects.addAll(list);
                    Log.d("[Map]", "Query successful.  Retrieved " + list.size() + " results.");
                    // Set up the filter for the data
                    setInitialFilter();
                    // Handle adding markers to the map and performing queries for the image data
                    manageData();
                    // Adjust to the map to contain the data points
                    if (!zoomed)
                        zoomMap();

                    // Store the group data for future referencing
                    if (currentGroupName == null || currentGroupName.equals(""))
                        storeGroupData(GPSSnapshot.getStringPref(getApplication(), "display_name"),
                                       objects, images);
                    else if (currentGroupName != null)
                        storeGroupData(currentGroupName, objects, images);

                }
            }
        });
    }

    /**
     * Sets the default filter options for the current data set, and gets list of all unique users
     * in the data set.
     */
    private void setInitialFilter()
    {
        if (filterNames == null)
            filterNames = new LinkedHashMap<>();
        else
            filterNames.clear();

        // Go through all objects and add all unique display names to filter list
        for (ParseObject obj : objects)
        {
            String name = obj.getString("display_name");
            if (name == null || name.equals(""))
                name = "[Anonymous]";
            if (!filterNames.containsKey(name))
            {
                filterNames.put(name, true);
            }
        }
        // Change the filter options fragment to reflect the new list of users to filter
        updateUserFilterListView();
    }

    /**
     * Goes through the current data set and adds their markers and queries for their image data.
     */
    private void manageData()
    {
        // Remove existing markers from the map
        clearMarkers();

        // Iterate through the current data set
        for (int i = 0; i < objects.size(); i++)
        {
            ParseObject obj = objects.get(i);

            // Change blank names
            if (obj.getString("display_name") == null || obj.getString("display_name").equals(""))
                obj.put("display_name", "[Anonymous]");

            // Add a marker to the map
            final Marker newMarker = addMarker(obj);

            // Get the image
            ParseFile parseFile = obj.getParseFile(UploadReceiver.PIC_KEY);

            // If the activity is intending to zoom on a marker immediately, be sure it does so
            // after the image has been loaded successfully.  Otherwise, just download the image
            // normally.
            if (intentZoomId != null && obj.getObjectId().equals(intentZoomId))
            {
                images.add(ImageHandler.newFromParse(this, parseFile, new ImageResponse()
                {
                    @Override
                    public void onSuccess()
                    {

                        zoomMap();
                        zoomToMarker(newMarker);
                    }

                    @Override
                    public void onFailure(int errorCode)
                    {

                    }
                }));
            }
            else
            {
                // Add new Image Handler to the array and queries Parse for its data
                images.add(ImageHandler.newFromParse(this, parseFile));
            }

        }
    }

    /**
     * Adds a marker on the google map at the geolocation provided by the given Parse Object.
     *
     * @param obj Parse Object whose geolocation to use.
     */
    private Marker addMarker(ParseObject obj)
    {
        if (map==null)
            return null;

        double lati = obj.getParseGeoPoint("geopoint").getLatitude();
        double longi = obj.getParseGeoPoint("geopoint").getLongitude();


        // Add the marker
        Marker newMarker = map.addMarker(new MarkerOptions()
                                                 .position(new LatLng(lati, longi))
                                                 .title(obj.getString("display_name")));

        markers.add(newMarker);

        // If the geolocation is null
        if (lati == 0 && longi == 0)
        {
            markers.get(markers.size()-1).setVisible(false);
        }

        return newMarker;
    }

    /**
     * Zooms the map to exactly fit all map markers.
     */
    private void zoomMap()
    {
        zoomed = true;

        if (map == null || objects == null || objects.size() == 0)
            return;

        LatLngBounds.Builder bounds = new LatLngBounds.Builder();

        // Iterate through all markers and add them to the given bounds if they are not [0,0] and
        //  if they are not invisible
        int counter = 0;
        for (Marker marker : markers)
        {
            if (marker.isVisible() &&
                !(marker.getPosition().latitude == 0 && marker.getPosition().longitude == 0))
            {
                bounds.include(marker.getPosition());
                counter++;
            }
        }

        if (counter > 0)
        {
            map.moveCamera(
                    CameraUpdateFactory.newLatLngBounds(bounds.build(),
                                                        getResources().getDimensionPixelSize(
                                                                R.dimen.MapViewer_MapPadding)));
        }

    }

    /**
     * Remove all markers from the map and from the marker list.
     */
    private void clearMarkers()
    {
        for (Marker marker : markers)
        {
            marker.remove();
        }
        markers.clear();
    }

    /**
     * Handles storing group data for later reference.
     *
     * @param name        Name of the group to store
     * @param groupObjs   Reference to the data used by the group
     * @param groupImages Reference to the images used by the group
     */
    private void storeGroupData(String name, ArrayList<ParseObject> groupObjs,
                                ArrayList<ImageHandler> groupImages)
    {
        groups.put(name, new GroupResults(name, groupObjs, groupImages));
    }

    /**
     * Sets up all UI features of the filter options fragment.
     */
    private void setUpFilterOptions()
    {
        View fragView = findViewById(R.id.MapViewer_FilterFragment);
        filterStartDate = Calendar.getInstance();
        filterEndDate = Calendar.getInstance();
        filterStartDate.setTimeInMillis(0);

        final EditText startTimeBox = (EditText) fragView.findViewById(R.id.FilterFragment_StartTimeInput);
        final EditText endTimeBox = (EditText) fragView.findViewById(R.id.FilterFragment_EndTimeInput);
        ListView nameListBox = (ListView) fragView.findViewById(R.id.FilterFragment_UserList);
        Spinner groupBox = (Spinner) fragView.findViewById(R.id.FilterFragment_GroupSpinner);
        CheckBox urgentBox = (CheckBox) fragView.findViewById(R.id.FilterFragment_IsUrgent);

        // Add date-picker dialogs to date inputs
        startTimeBox.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    if (filterStartDate.getTimeInMillis() == 0)
                    {
                        filterStartDate = Calendar.getInstance();
                    }
                    new DatePickerDialog(
                            MapActivity.this,
                            new DatePickerDialog.OnDateSetListener()
                            {
                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth)
                                {
                                    filterStartDate.set(Calendar.YEAR, year);
                                    filterStartDate.set(Calendar.MONTH, monthOfYear);
                                    filterStartDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    updateLabel(startTimeBox, filterStartDate);
                                    applyFilter();
                                }


                            },
                            filterStartDate.get(Calendar.YEAR),
                            filterStartDate.get(Calendar.MONTH),
                            filterStartDate.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });

        endTimeBox.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                if (event.getAction() == MotionEvent.ACTION_UP)
                {

                    new DatePickerDialog(
                            MapActivity.this,
                            new DatePickerDialog.OnDateSetListener()
                            {
                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth)
                                {
                                    filterEndDate.set(Calendar.YEAR, year);
                                    filterEndDate.set(Calendar.MONTH, monthOfYear);
                                    filterEndDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                    updateLabel(endTimeBox, filterEndDate);
                                    applyFilter();
                                }
                            },
                            filterEndDate.get(Calendar.YEAR),
                            filterEndDate.get(Calendar.MONTH),
                            filterEndDate.get(Calendar.DAY_OF_MONTH)).show();
                }
                return true;
            }
        });

        filterUrgentOnly = false;
        urgentBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                filterUrgentOnly = isChecked;
                applyFilter();
            }
        });

        // Attach adapter and item click listener to list view.  List is populated after data is
        //  retrieved from Parse.
        filterNames = new LinkedHashMap<>();
        nameListBox.setAdapter(new UserFilterListAdapter(this, filterNames));
        nameListBox.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String[] keys = filterNames.keySet().toArray(new String[filterNames.size()]);
                filterNames.put(keys[position], !filterNames.get(keys[position]));
                ((UserFilterListAdapter) parent.getAdapter()).notifyDataSetChanged();
                applyFilter();
            }
        });


        // Set up group-picker Spinner with My Groups

        final ArrayList<String> myGroupNames = new ArrayList<>();
        // Get names of all groups and add to array
        for (DataGroup dataGroup : GPSSnapshot.getMyGroups(this))
            myGroupNames.add(dataGroup.title);
        // Set up basic adapter
        ArrayAdapter<String> groupBoxAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, myGroupNames);
        groupBoxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupBox.setAdapter(groupBoxAdapter);

        // Change group selection to match the current group by default
        if (myGroupNames.indexOf(currentGroupName) != -1)
            groupBox.setSelection(myGroupNames.indexOf(currentGroupName));

        // Apply on item selected listener to change groups
        groupBox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Log.d("[Map]", "Group '" + myGroupNames.get(position)
                               + "' (" + (position + 1) + "/" + myGroupNames.size() + ") selected.");
                currentGroupName = myGroupNames.get(position);
                zoomed = false;
                makeParseQueryWithGroup(myGroupNames.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
                makeParseQueryWithUser();
            }
        });


    }

    /**
     * Handles updating the list view containing the list of names to filter
     */
    private void updateUserFilterListView()
    {
        View fragView = findViewById(R.id.MapViewer_FilterFragment);
        if (fragView != null)
        {
            ListView nameListBox = (ListView) fragView.findViewById(R.id.FilterFragment_UserList);
            if (nameListBox != null && nameListBox.getAdapter() != null)
                ((UserFilterListAdapter) nameListBox.getAdapter()).notifyDataSetChanged();
        }
    }

    /**
     * Updates the given Edit Text box with a formatted version of the given calendar
     *
     * @param view EditText to change
     * @param date Date to format.
     */
    private void updateLabel(EditText view, Calendar date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy", Locale.UK);
        view.setText(sdf.format(date.getTime()));
    }

    /**
     * Handles deciding which markers to show or to not show based on the current filter options.
     */
    private void applyFilter()
    {
        // Iterate through all markers/objects
        for (int i = 0; i < markers.size(); i++)
        {
            Marker marker = markers.get(i);
            ParseObject obj = objects.get(i);

            // These variables must all end up true in order for the marker to be visible
            boolean nameMatch, dateMatch, urgentMatch;

            String name = obj.getString("display_name");
            // The name is a match if either the name is null, or if the name is on the filter list
            //  and is checked
            nameMatch = (name == null) || (filterNames.containsKey(name) && filterNames.get(name));

            urgentMatch = !filterUrgentOnly || (obj.has("urgent") && obj.getBoolean("urgent"));

            Date date = obj.getDate("time");
            // Set start and end filter dates to encompass the entirety of their days
            filterStartDate.set(Calendar.HOUR_OF_DAY, 0);
            filterStartDate.set(Calendar.MINUTE, 0);
            filterStartDate.set(Calendar.SECOND, 0);
            filterEndDate.set(Calendar.HOUR_OF_DAY, 23);
            filterEndDate.set(Calendar.MINUTE, 59);
            filterEndDate.set(Calendar.SECOND, 59);
            dateMatch = (date.after(filterStartDate.getTime())
                         && date.before(filterEndDate.getTime()));

            // Make the marker visible if the marker matches the name filter and date range
            marker.setVisible(nameMatch && dateMatch && urgentMatch);

        }

        // After filtering, zoom the map
        zoomMap();
    }

    private void testForFirstTimeUse()
    {
        if (!GPSSnapshot.getBoolPref(this, "first_map"))
        {
            new ShowcaseView.Builder(this)
                    .setTarget(new ViewTarget(R.id.MapViewer_FilterToggle, this))
                    .setContentTitle(R.string.MapsTutorial_FilterTitle)
                    .setContentText(R.string.MapsTutorial_FilterText)
                    .setStyle(R.style.DarkTutorial)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView)
                        {
                            GPSSnapshot.setBoolPref(getApplicationContext(), "first_map", true);
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                        {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView)
                        {

                        }
                    })
                    .hideOnTouchOutside()
                    .build();
        }
    }

    /**
     * Called from filter options fragment's onStart method, signals setting up the fragment.
     */
    @Override
    public void onFragmentLayoutComplete()
    {
        setUpFilterOptions();
    }
}
