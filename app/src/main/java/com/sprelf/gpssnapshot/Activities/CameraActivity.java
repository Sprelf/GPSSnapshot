package com.sprelf.gpssnapshot.Activities;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sprelf.gpssnapshot.CustomViews.CameraPreview;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.Handlers.PhotoHandler;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.Services.AppCloseService;
import com.sprelf.gpssnapshot.Services.GPSService;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;


public class CameraActivity extends Activity
{

    private Camera camera;
    private int cameraId = 0;
    private CameraPreview preview;

    private boolean isCameraOpen = false;

    private long pictureTime = 0;

    private MediaPlayer mediaPlayer;

    private AsyncTask<String, String, String> cameraOpener;

    private static int VIEW_ROTATE_DURATION = 350;  // in milliseconds

    private MyOrientationEventListener myOrientationEventListener;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        myOrientationEventListener = new MyOrientationEventListener(this);
        myOrientationEventListener.enable();

        // Start background service for uploading data
        GPSSnapshot.startUpdateService(this);
        GPSSnapshot.startTrackingService(this);
        GPSSnapshot.startPictureCleanupService(this);
        startService(new Intent(this, AppCloseService.class));

        Log.d("[Debug]", "onCreate");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.d("[Debug]", "onDestroy");
    }

    @Override
    protected void onResume()
    {
        super.onResume();


        /*
        // Determine if user name for this device exists
        SharedPreferences settings = getSharedPreferences(GPSSnapshot.SHAREDPREFS_FILE, 0);
        String username = settings.getString(GPSSnapshot.USERNAME_KEY, null);

        // If not, ask for a user name
        if (username == null)
        {
            queryUsername();
        }
        */

        // Test for camera
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            Log.e("[Camera]", "No camera on this device");
        }
        else
        {
            // Find the ID of the rear-facing camera.
            cameraId = findCamera();
            if (cameraId < 0)
            {
                Log.e("[Camera]", "No rear-facing camera.");
            }
            else
            {
                // Open the camera and start the camera preview
                cameraOpener = new AsyncTask<String, String, String>()
                {
                    @Override
                    protected String doInBackground(String... params)
                    {
                        Log.d("[Camera]", "Attempting to open camera...");
                        if (camera == null)
                            camera = Camera.open(cameraId);
                        else
                            Log.d("[Camera]", "Camera was already active, not re-opening.");
                        isCameraOpen = true;

                        generatePreview();
                        return null;
                    }

                    @Override
                    protected void onCancelled()
                    {
                        super.onCancelled();
                        Log.d("[Camera]", "Camera opener cancelled.");
                    }

                    @Override
                    protected void onPostExecute(String s)
                    {
                        super.onPostExecute(s);
                        cameraOpener = null;
                    }

                };
                cameraOpener.execute();

            }
        }
        Log.d("[Debug]", "onResume");
    }

    @Override
    protected void onPause()
    {
        // Suspend the camera when the activity is paused
        if (camera != null)
        {
            preview.setCamera(null);
            camera.release();
            camera = null;
            if (cameraOpener != null)
                cameraOpener.cancel(true);
            isCameraOpen = false;
        }
        if (mediaPlayer != null)
            mediaPlayer.release();

        super.onPause();
        Log.d("[Debug]", "onPause");
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Log.d("[Debug]", "onStart");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Camera Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.sprelf.gpssnapshot.Activities/http/host/path")
                                            );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Camera Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.sprelf.gpssnapshot.Activities/http/host/path")
                                            );
        AppIndex.AppIndexApi.end(client, viewAction);
        Log.d("[Debug]", "onStop");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    /*
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            Log.d("[Debug]","Landscape");
        }
        else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
        {
            Log.d("[Debug]","Portrait");
        }
    }
    */

    /**
     * OnClick method for the shutter, redirecting to appropriate code.
     *
     * @param view Reference to View that was clicked
     */
    public void onShutterClick(View view)
    {
        if (System.currentTimeMillis() - pictureTime >=
            GPSSnapshot.getIntPref(this, "image_delay") * 1000)
        {
            takePicture();
            pictureTime = System.currentTimeMillis();
        }
    }

    /**
     * DEPRECATED
     * <p/>
     * public void onChangeUserClick(View view)
     * {
     * queryUsername();
     * }
     */
    public void onSettingsClick(View view)
    {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    /* DEPRECATED
    private void queryUsername()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Input name");

        // Set up the input
        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                SharedPreferences.Editor editor = getApplicationContext()
                        .getSharedPreferences(GPSSnapshot.SHAREDPREFS_FILE, 0).edit();
                editor.putString(GPSSnapshot.USERNAME_KEY, input.getText().toString());
                editor.commit();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.cancel();
            }
        });

        builder.show();
    }
    */

    /**
     * Handles all actions associate with taking a picture with the camera, including collection
     * of GPS and time data and saving to SQL database.
     */
    private void takePicture()
    {

        if (!isCameraOpen)
            return;

        // Get picture name and snap picture
        final String picPath = GPSSnapshot.getDir(this).getPath() + File.separator
                               + GPSSnapshot.DATE_FORMAT.format(new Date()) + ".jpg";
        camera.takePicture(null, null, new PhotoHandler(getApplicationContext(), picPath));

        // Perform shutter press confirmation actions
        shutterClick();

        // Get current time
        final String timeString = GPSSnapshot.DATE_FORMAT.format(new Date());

        // Set up dialog window for description input
        final View dialogView = View.inflate(this, R.layout.layout_post_photo_dialog, null);

        final ArrayList<String> myGroupNames = new ArrayList<>();
        for (DataGroup dataGroup : GPSSnapshot.getMyGroups(this))
            myGroupNames.add(dataGroup.title);
        ArrayAdapter<String> groupBoxAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, myGroupNames);
        groupBoxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        Spinner groupSpinner = (Spinner) dialogView.findViewById(R.id.PostPhotoDialog_GroupSpinner);
        groupSpinner.setAdapter(groupBoxAdapter);
        DataGroup currGroup = GPSSnapshot.getCurrentGroup(this);
        if (currGroup != null && myGroupNames.contains(currGroup.title))
            groupSpinner.setSelection(myGroupNames.indexOf(currGroup.title));


        // Open dialog window for description input
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView)
               .setMessage(R.string.PostPhotoDialog_Message)
               .setPositiveButton(R.string.PostPhotoDialog_Confirm,
                                  new DialogInterface.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which)
                                      {
                                          String desc = ((EditText) dialogView.findViewById(
                                                  R.id.PostPhotoDialog_DescriptionInput))
                                                  .getText().toString();
                                          Object currGroup = ((Spinner) dialogView.findViewById(
                                                  R.id.PostPhotoDialog_GroupSpinner))
                                                  .getSelectedItem();
                                          CheckBox isUrgent = (CheckBox) dialogView.findViewById(
                                                  R.id.PostPhotoDialog_IsUrgent);

                                          String group = (currGroup != null) ?
                                                  currGroup.toString() : "Public";

                                          // Start service for obtaining GPS location.
                                          // Data is submitted when this finalizes.
                                          Intent intent = new Intent(CameraActivity.this,
                                                                     GPSService.class);
                                          intent.putExtra("picPath", picPath);
                                          intent.putExtra("time", timeString);
                                          intent.putExtra("description", desc);
                                          intent.putExtra("groupName", group);
                                          intent.putExtra("urgent", isUrgent.isChecked());
                                          startService(intent);
                                      }
                                  })
               .setNegativeButton(R.string.PostPhotoDialog_Cancel,
                                  new DialogInterface.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which)
                                      {
                                          // Discard the image
                                          GPSSnapshot.deleteImage(getApplicationContext(), picPath);
                                      }
                                  });
        builder.show();


    }

    /**
     * Performs all actions for providing confirmation feedback on press of the shutter.
     */
    private void shutterClick()
    {
        // Play shutter click sound
        mediaPlayer = MediaPlayer.create(this, R.raw.camera_shutter_click_01);
        mediaPlayer.start();
    }


    /**
     * Identifies the rear-facing camera and returns the camera's ID.
     *
     * @return ID of the rear-facing camera.  Returns -1 if no such camera exists.
     */
    private int findCamera()
    {
        int cameraId = -1;
        // Find rear-facing camera
        int numCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numCameras; i++)
        {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
            {
                Log.d("[Camera]", "Camera found");
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    /**
     * Initializes the preview View in the layout for displaying what the camera sees.
     */
    private void generatePreview()
    {
        if (!isCameraOpen)
            return;

        Camera.Parameters camParams = camera.getParameters();
        if (!camParams.getSupportedFocusModes().isEmpty())
        {
            if (camParams.getSupportedFocusModes()
                         .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            {
                Log.d("[Camera]", "Setting continuous picture focus mode.");
                camParams.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
            else if (camParams.getSupportedFocusModes()
                              .contains(Camera.Parameters.FOCUS_MODE_AUTO))
            {
                Log.d("[Camera]", "Setting auto focus mode.");
                camParams.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
            else
            {
                Log.d("[Camera]", "Setting an arbitrary focus mode:  "
                                  + camParams.getSupportedFocusModes().get(0));
                camParams.setFocusMode(camParams.getSupportedFocusModes().get(0));
            }
            camera.setParameters(camParams);
        }
        else
        {
            Log.d("[Camera]", "No focus modes supported.");
            // Don't apply a focus mode... it doesn't support any.  For some reason.
        }


        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.RootLayout);
        preview = new CameraPreview(this);
        preview.setCamera(camera);
        layout.addView(preview, params);

        findViewById(R.id.Camera_Button).bringToFront();
        findViewById(R.id.Settings_Button).bringToFront();

        //Test for first time use
        testForFirstTimeUse();
    }


    private void testForFirstTimeUse()
    {
        if (!GPSSnapshot.getBoolPref(this, "first_camera"))
        {
            final View shutterButton = findViewById(R.id.Camera_Button);

            new ShowcaseView.Builder(this)
                    .setTarget(new ViewTarget(findViewById(R.id.Settings_Button)))
                    .setContentTitle(R.string.CameraTutorial_SettingsTitle)
                    .setContentText(R.string.CameraTutorial_SettingsText)
                    .hideOnTouchOutside()
                    .setShowcaseEventListener(new OnShowcaseEventListener()
                    {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView)
                        {
                            shutterButton.setVisibility(View.VISIBLE);
                            new ShowcaseView.Builder(CameraActivity.this)
                                    .setTarget(new ViewTarget(shutterButton))
                                    .setContentTitle(R.string.CameraTutorial_CameraTitle)
                                    .setContentText(R.string.CameraTutorial_CameraText)
                                    .setShowcaseEventListener(new OnShowcaseEventListener()
                                    {
                                        @Override
                                        public void onShowcaseViewHide(ShowcaseView showcaseView)
                                        {
                                            GPSSnapshot.setBoolPref(getApplicationContext(),
                                                                    "first_camera", true);
                                        }

                                        @Override
                                        public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                                        {

                                        }

                                        @Override
                                        public void onShowcaseViewShow(ShowcaseView showcaseView)
                                        {

                                        }
                                    })
                                    .hideOnTouchOutside()
                                    .build();
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                        {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView)
                        {
                            shutterButton.setVisibility(View.INVISIBLE);
                        }
                    })
                    .build();


        }
    }

    private class MyOrientationEventListener extends OrientationEventListener
    {
        private int oldValue = 0;

        MyOrientationEventListener(Context context)
        {
            super(context);
        }

        @Override
        public void onOrientationChanged(int orientation)
        {
            ImageView settingsButton = (ImageView) findViewById(R.id.Settings_Image);
            if ((orientation > 315 || orientation <= 45) ||
                (orientation > 135 && orientation <= 225))
            {
                if (settingsButton.getRotation() == 0f || settingsButton.getRotation() == 360f)
                {
                    float startValue = oldValue > orientation ? 0f : 180f;

                    ObjectAnimator anim = ObjectAnimator.ofFloat(settingsButton, "Rotation",
                                                                 startValue, 90f);
                    anim.setDuration(VIEW_ROTATE_DURATION);
                    anim.start();
                }
            }
            else
            {
                if (settingsButton.getRotation() == 90f)
                {
                    float startValue = oldValue > orientation ? 270f : 90f;
                    float endValue = oldValue > orientation ? 360f : 0f;

                    ObjectAnimator anim = ObjectAnimator.ofFloat(settingsButton, "Rotation",
                                                                 startValue, endValue);
                    anim.setDuration(VIEW_ROTATE_DURATION);
                    anim.start();
                }
            }

            oldValue = orientation;
        }
    }


}
