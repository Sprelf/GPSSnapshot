package com.sprelf.gpssnapshot.Activities;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.Receivers.UploadReceiver;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;
import com.sprelf.gpssnapshot.UtilityClasses.LogInResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ImageViewerActivity extends Activity
{

    TextView gpsText, timeText;
    ImageView image;
    ParseUser user;
    List<ParseObject> objects;
    ArrayList<byte[]> images;
    String currentGroupName;

    int currIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_image_viewer);

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        actionBar.setDisplayHomeAsUpEnabled(true);

        gpsText = (TextView) findViewById(R.id.ImageViewer_GPSText);
        timeText = (TextView) findViewById(R.id.ImageViewer_TimeText);
        image = (ImageView) findViewById(R.id.ImageViewer_ImageArea);

        // This is currently global, but probably unnecessary for it to be.
        currentGroupName = GPSSnapshot.getStringPref(this, "currgroup_name");

        if (!GPSSnapshot.isInternetConnected(this))
            GPSSnapshot.generateErrorDialog(this, GPSSnapshot.ERROR_NO_INTERNET, null);
        else // Log in user and start Parse Queries
            user = logInUserAndQuery();

        images = new ArrayList<>();


    }

    public void onImageClick(View view)
    {
        cycleImage();
    }

    private void cycleImage()
    {
        currIndex++;
        if (currIndex >= images.size())
            currIndex = 0;
        showImage(currIndex);
    }


    private void makeParseQueryWithUser()
    {
        Log.d("[Image Viewer]", "Querying Parse for all images from this user...");

        ParseQuery<ParseObject> innerQuery = ParseQuery.getQuery("_User");
        innerQuery.whereEqualTo("objectId", user.getObjectId());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("DataPoint");
        query.whereMatchesQuery(UploadReceiver.USER_KEY, innerQuery);
        launchQuery(query);
    }

    private void makeParseQueryWithGroup(String groupName, boolean isPublic, boolean isAdmin)
    {
        Log.d("[Image Viewer]", "Querying Parse for all images in the group '" + groupName + "'.");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("DataPoint");
        query.whereEqualTo(UploadReceiver.GROUP_NAME_KEY, groupName);
        if (!isPublic && !isAdmin)
            query.whereEqualTo(UploadReceiver.USER_KEY, ParseUser.getCurrentUser());
        launchQuery(query);
    }

    private void launchQuery(ParseQuery<ParseObject> query)
    {
        query.findInBackground(new FindCallback<ParseObject>()
        {
            @Override
            public void done(List<ParseObject> list, ParseException e)
            {
                if (e != null)
                {
                    Log.e("[Image Viewer]", "Error with Parse query.\n" + e.getMessage() +
                                            "\nError Code = " + e.getCode());
                    e.printStackTrace();
                }
                else
                {
                    if (list==null)
                        objects = new ArrayList<ParseObject>();
                    else
                        objects = list;
                    Log.d("[Image Viewer]", "Query successful.  Retrieved " +
                                            list.size() + " results.");
                    downloadImages(0);
                }
            }
        });
    }

    private void downloadImages(final int showWhenDone)
    {
        if (objects==null)
            return;

        for (int i = 0; i < objects.size(); i++)
        {
            ParseObject obj = objects.get(i);
            ParseFile parseFile = obj.getParseFile(UploadReceiver.PIC_KEY);

            final int index = i;

            images.add(new byte[]{});

            parseFile.getDataInBackground(new GetDataCallback()
            {
                @Override
                public void done(byte[] bytes, ParseException e)
                {
                    if (e != null)
                    {
                        Log.e("[Image Viewer]", "Error retrieving image data.\n" + e.getMessage() +
                                                "\nError Code = " + e.getCode());
                    }
                    else
                    {
                        Log.d("[Image Viewer]", "Retrieved image #" + index);
                        images.set(index, bytes);
                        if (index == showWhenDone)
                            showImage(index);
                    }
                }
            });
        }
    }

    private void showImage(int index)
    {
        if (objects==null)
            return;

        if (objects.size() > 0)
        {
            index %= objects.size();

            Log.d("[Image Viewer]", "Displaying image #" + index);

            ParseObject obj = objects.get(index);

            String gpsString = "[" + Double.toString(obj.getParseGeoPoint("geopoint").getLatitude())
                               + ", " + Double.toString(obj.getParseGeoPoint("geopoint").getLongitude()) + "]";
            gpsText.setText(gpsString);

            timeText.setText(new SimpleDateFormat("dd MMM, yyyy HH:mm:ss").format(
                    obj.getDate(UploadReceiver.TIME_KEY)));

            if (images.get(index) != null)
            {
                image.setImageBitmap(BitmapFactory.decodeByteArray(
                        images.get(index), 0, images.get(index).length));
            }

        }
    }

    /**
     * Handles logging the user into Parse.  If successful, calls appropriate Parse data queries.
     *
     * @return Reference to the current user after logging in.
     */
    private ParseUser logInUserAndQuery()
    {
        GPSSnapshot.logInUser(this, new LogInResponse()
        {
            @Override
            public void onSuccess()
            {
                user = ParseUser.getCurrentUser();
                if (currentGroupName.equals(""))
                {
                    makeParseQueryWithUser();
                }
                else
                {
                    DataGroup currentGroup = GPSSnapshot.getGroupByName(ImageViewerActivity.this,
                                                                        currentGroupName);
                    if (currentGroup == null)
                        makeParseQueryWithGroup(currentGroupName, false, false);
                    else
                        makeParseQueryWithGroup(currentGroupName, currentGroup.publicEntries,
                                                currentGroup.isAdmin);
                }
            }

            @Override
            public void onInvalid()
            {
                GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                GPSSnapshot.ERROR_INVALID_LOGIN, null);
            }

            @Override
            public void onFailure(int errorCode)
            {
                GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                GPSSnapshot.ERROR_BAD_LOGIN, null);
            }
        });

        return ParseUser.getCurrentUser();
    }


}


