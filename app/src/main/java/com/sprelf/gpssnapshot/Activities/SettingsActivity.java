package com.sprelf.gpssnapshot.Activities;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.parse.ParseUser;
import com.sprelf.gpssnapshot.CustomPreferences.CustomPreference;
import com.sprelf.gpssnapshot.CustomTargets.OffsetViewTarget;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.LogInResponse;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity implements
                                                         CustomPreference.CustomPrefBindingListener
{
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;

    private View[] tutorialViews;

    private static final int TUTORIAL_MAPVIEW = 0;
    private static final int TUTORIAL_DISPLAYNAME = 1;
    private static final int TUTORIAL_USERLOGIN = 2;
    private static final int TUTORIAL_GROUPCONNECTION = 3;
    private static final int TUTORIAL_NOTIFICATIONS = 4;
    private static final int TUTORIAL_OTHER = 5;
    private static final int TUTORIAL_OVER = 7;


    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);

        setupSimplePreferencesScreen();

        ActionBar actionBar = getActionBar();
        actionBar.setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        actionBar.setDisplayHomeAsUpEnabled(true);

        tutorialViews = new View[TUTORIAL_OVER + 1];
    }


    @Override
    public void onStop()
    {
        super.onStop();
        GPSSnapshot.startPictureCleanupService(this);
        if (GPSSnapshot.getBoolPref(this, "enable_tracking"))
            GPSSnapshot.startTrackingService(this);
        else
            GPSSnapshot.stopTrackingService(this);
        GPSSnapshot.startUpdateService(this);
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    private void setupSimplePreferencesScreen()
    {
        if (!isSimplePreferences(this))
        {
            return;
        }

        // Data Viewing
        addPreferencesFromResource(R.xml.pref_data_viewing);

        // User Options
        PreferenceCategory tempHeader = new PreferenceCategory(this);
        tempHeader.setTitle(R.string.pref_header_user_options);
        getPreferenceScreen().addPreference(tempHeader);
        addPreferencesFromResource(R.xml.pref_user_options);

        // Upload Options
        tempHeader = new PreferenceCategory(this);
        tempHeader.setTitle(R.string.pref_header_upload_options);
        getPreferenceScreen().addPreference(tempHeader);
        addPreferencesFromResource(R.xml.pref_upload_options);

        // Tracking Options
        tempHeader = new PreferenceCategory(this);
        tempHeader.setTitle(R.string.pref_header_tracking_options);
        getPreferenceScreen().addPreference(tempHeader);
        addPreferencesFromResource(R.xml.pref_tracking_options);

        // Image Options
        tempHeader = new PreferenceCategory(this);
        tempHeader.setTitle(R.string.pref_header_image_options);
        getPreferenceScreen().addPreference(tempHeader);
        addPreferencesFromResource(R.xml.pref_image_options);

        // Other Options
        tempHeader = new PreferenceCategory(this);
        tempHeader.setTitle(R.string.pref_header_other_options);
        getPreferenceScreen().addPreference(tempHeader);
        addPreferencesFromResource(R.xml.pref_other_options);


        bindPreferenceSummaryToValue(findPreference("display_name"));
        bindPreferenceSummaryToValue(findPreference("user_email"));
        bindPreferenceSummaryToValue(findPreference("user_password"));
        bindPreferenceSummaryToValue(findPreference("user_phone"));

        bindPreferenceSummaryToValue(findPreference("enable_tracking"));
        bindPreferenceSummaryToValue(findPreference("tracking_freq"));
        bindPreferenceSummaryToValue(findPreference("tracking_accuracy"));
        bindPreferenceSummaryToValue(findPreference("tracking_timeout"));

        bindPreferenceSummaryToValue(findPreference("image_accuracy"));
        bindPreferenceSummaryToValue(findPreference("image_timeout"));
        bindPreferenceSummaryToValue(findPreference("image_delay"));
        bindPreferenceSummaryToValue(findPreference("image_retention"));

        bindPreferenceSummaryToValue(findPreference("upload_type"));
        bindPreferenceSummaryToValue(findPreference("upload_freq"));
        bindPreferenceSummaryToValue(findPreference("upload_size"));

        bindPreferenceSummaryToValue(findPreference("notifications_enabled"));
        bindPreferenceSummaryToValue(findPreference("notifications_sound"));

        ((Preference) findPreference("log_in"))
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(Preference preference)
                    {

                        // Enable anonymous users
                        ParseUser.enableAutomaticUser();

                        GPSSnapshot.logOutUser(getApplicationContext(), new LogInResponse()
                        {
                            @Override
                            public void onSuccess()
                            {
                                GPSSnapshot.logInUser(getApplicationContext(), new LogInResponse()
                                {
                                    @Override
                                    public void onSuccess()
                                    {
                                        Toast.makeText(getApplicationContext(),
                                                       getString(R.string.pref_log_in_success),
                                                       Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onInvalid()
                                    {
                                        GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                                        GPSSnapshot.ERROR_INVALID_LOGIN,
                                                                        null);
                                    }

                                    @Override
                                    public void onFailure(int errorCode)
                                    {
                                        GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                                        GPSSnapshot.ERROR_BAD_LOGIN,
                                                                        null);
                                    }
                                });
                            }

                            @Override
                            public void onInvalid()
                            {

                            }

                            @Override
                            public void onFailure(int errorCode)
                            {

                            }
                        });


                        return false;
                    }
                });
        ((Preference) findPreference("reset_tutorials")).setOnPreferenceClickListener(
                new Preference.OnPreferenceClickListener()
                {
                    @Override
                    public boolean onPreferenceClick(Preference preference)
                    {
                        GPSSnapshot.resetTutorials(getApplicationContext());

                        return false;
                    }
                }
                                                                                     );


    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane()
    {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context)
    {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context)
    {
        return ALWAYS_SIMPLE_PREFS
               || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
               || !isXLargeTablet(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target)
    {
        if (!isSimplePreferences(this))
        {
            loadHeadersFromResource(R.xml.pref_headers, target);
        }
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener()
    {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value)
        {
            String stringValue = value.toString();
            Context c = preference.getContext();

            if (preference instanceof ListPreference)
            {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                switch (preference.getKey())
                {
                    case "upload_type":
                        break;
                    case "image_retention":
                        break;
                }

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            }
            else
            {
                String summaryString;
                switch (preference.getKey())
                {
                    case "tracking_freq":
                        summaryString = c.getString(R.string.pref_freq_string)
                                        + " " + stringValue + " "
                                        + c.getString(R.string.unit_minutes);
                        break;
                    case "upload_freq":
                        summaryString = c.getString(R.string.pref_freq_string)
                                        + " " + stringValue + " "
                                        + c.getString(R.string.unit_minutes);
                        break;
                    case "tracking_timeout":
                    case "image_timeout":
                    case "image_delay":
                        summaryString = stringValue + " " + c.getString(R.string.unit_seconds);
                        break;
                    case "tracking_accuracy":
                    case "image_accuracy":
                        summaryString = stringValue + " " + c.getString(R.string.unit_meters);
                        break;
                    case "upload_size":
                        summaryString = stringValue + " " + c.getString(R.string.unit_kilobytes);
                        break;
                    case "enable_tracking":
                        summaryString = c.getString(R.string.pref_description_enable_tracking);


                        break;

                    case "user_email":
                        preference.getPreferenceManager()
                                  .findPreference("user_login").setSummary(stringValue);
                    case "display_name":
                        summaryString = stringValue;
                        break;
                    case "user_password":
                        summaryString = stringValue.equals("") ? "" : "******";
                        break;
                    case "user_phone":
                        summaryString = stringValue;
                        break;
                    case "notifications_enabled":
                        summaryString = c.getString(R.string.pref_description_notifications_enabled);
                        break;
                    case "notifications_sound":
                        summaryString = c.getString(R.string.pref_description_notifications_default_sound);
                        break;
                    default:
                        summaryString = "";
                }
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(summaryString);
            }
            return true;
        }
    };

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference)
    {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        if (preference instanceof CheckBoxPreference)
        {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getBoolean(preference.getKey(), false));
        }
        else
        {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(
                    preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
        }
    }

    @Override
    public void onBindView(View view, String key)
    {
        switch (key)
        {
            case "map_viewer":
                tutorialViews[TUTORIAL_MAPVIEW] = view;
                break;
            case "display_name":
                tutorialViews[TUTORIAL_DISPLAYNAME] = view;
                break;
            case "user_phone":
                tutorialViews[TUTORIAL_USERLOGIN] = view;
                break;
            case "group_connections":
                tutorialViews[TUTORIAL_GROUPCONNECTION] = view;
                break;
            case "upload_freq":
                tutorialViews[TUTORIAL_OTHER] = view;
                break;
            case "reset_tutorials":
                tutorialViews[TUTORIAL_NOTIFICATIONS] = view;
                break;
        }

        if (key.equals("map_viewer") && !GPSSnapshot.getBoolPref(this, "first_settings"))
        {

            final OnShowcaseEventListener done = new OnShowcaseEventListener()
            {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    GPSSnapshot.setBoolPref(getApplicationContext(), "first_settings", true);
                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };

            final OnShowcaseEventListener page5 = new OnShowcaseEventListener()
            {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    getListView().setSelection(findPreference("reset_tutorials").getOrder());
                    tutorialViews[TUTORIAL_NOTIFICATIONS] =
                            ((CustomPreference) findPreference("reset_tutorials")).getView();

                    if (tutorialViews[TUTORIAL_NOTIFICATIONS] != null)

                        new ShowcaseView.Builder(SettingsActivity.this)
                                .setTarget(new OffsetViewTarget(tutorialViews[TUTORIAL_NOTIFICATIONS],
                                                                0.28f, -0.20f))
                                .setContentTitle(R.string.SettingsTutorial_NotificationsTitle)
                                .setContentText(R.string.SettingsTutorial_NotificationsText)
                                .setShowcaseEventListener(done)
                                .setStyle(R.style.DarkTutorial)
                                .hideOnTouchOutside()
                                .build();

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };

            final OnShowcaseEventListener page4 = new OnShowcaseEventListener()
            {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    if (tutorialViews[TUTORIAL_OTHER] != null)
                        new ShowcaseView.Builder(SettingsActivity.this)
                                .setTarget(new OffsetViewTarget(tutorialViews[TUTORIAL_OTHER],
                                                                0.20f, 0.2f))
                                .setContentTitle(R.string.SettingsTutorial_OtherTitle)
                                .setContentText(R.string.SettingsTutorial_OtherText)
                                .setShowcaseEventListener(page5)
                                .setStyle(R.style.DarkTutorial)
                                .hideOnTouchOutside()
                                .build();

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };


            final OnShowcaseEventListener page2 = new OnShowcaseEventListener()
            {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    getListView().setSelection(findPreference("group_connections").getOrder());
                    if (tutorialViews[TUTORIAL_GROUPCONNECTION] != null)
                        new ShowcaseView.Builder(SettingsActivity.this)
                                .setTarget(new OffsetViewTarget(
                                        tutorialViews[TUTORIAL_GROUPCONNECTION], 0.25f, 0.5f))
                                .setContentTitle(R.string.SettingsTutorial_GroupTitle)
                                .setContentText(R.string.SettingsTutorial_GroupText)
                                .setShowcaseEventListener(page4)
                                .setStyle(R.style.DarkTutorial)
                                .hideOnTouchOutside()
                                .build();

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };
            final OnShowcaseEventListener page1 = new OnShowcaseEventListener()
            {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    getListView().setSelection(findPreference("display_name").getOrder());
                    if (tutorialViews[TUTORIAL_DISPLAYNAME] != null)
                        new ShowcaseView.Builder(SettingsActivity.this)
                                .setTarget(new OffsetViewTarget(tutorialViews[TUTORIAL_DISPLAYNAME],
                                                                0.08f, 0.9f))
                                .setContentTitle(R.string.SettingsTutorial_NameTitle)
                                .setContentText(R.string.SettingsTutorial_NameText)
                                .setShowcaseEventListener(page2)
                                .hideOnTouchOutside()
                                .setStyle(R.style.DarkTutorial)
                                .build();

                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };

            new Handler().post(new Runnable()
            {
                @Override
                public void run()
                {
                    getListView().setSelection(findPreference("reset_tutorials").getOrder());
                    getListView().setSelection(findPreference("map_viewer").getOrder());
                    new ShowcaseView.Builder(SettingsActivity.this)
                            .setTarget(new OffsetViewTarget(tutorialViews[TUTORIAL_MAPVIEW],
                                                            0.15f, 0.5f))
                            .setContentTitle(R.string.SettingsTutorial_MapTitle)
                            .setContentText(R.string.SettingsTutorial_MapText)
                            .hideOnTouchOutside()
                            .setShowcaseEventListener(page1)
                            .setStyle(R.style.DarkTutorial)
                            .build();
                }
            });

        }
        else if (key.equals("user_phone") && !GPSSnapshot.getBoolPref(this, "first_settings_user"))
        {
            /*Log.d("[Settings]", "User settings opened.");
            tutorialViews[TUTORIAL_USERLOGIN] = view;
            if (tutorialViews[TUTORIAL_USERLOGIN] != null)
                new Handler().post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        new ShowcaseView.Builder(SettingsActivity.this)
                                .setTarget(new OffsetViewTarget(
                                        tutorialViews[TUTORIAL_USERLOGIN], 0.25f, 0f))
                                .setContentTitle(R.string.SettingsTutorial_UserInfoTitle)
                                .setContentText(R.string.SettingsTutorial_UserInfoText)
                                .setShowcaseEventListener(new OnShowcaseEventListener()
                                {
                                    @Override
                                    public void onShowcaseViewHide(ShowcaseView showcaseView)
                                    {
                                        GPSSnapshot.setBoolPref(getApplicationContext(),
                                                                "first_settings_user", true);
                                    }

                                    @Override
                                    public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                                    {

                                    }

                                    @Override
                                    public void onShowcaseViewShow(ShowcaseView showcaseView)
                                    {

                                    }
                                })
                                .setStyle(R.style.DarkTutorial)
                                .hideOnTouchOutside()
                                .build();
                    }
                });*/


        }

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DataViewingFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_data_viewing);

        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class UserOptionsFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_user_options);

            bindPreferenceSummaryToValue(findPreference("display_name"));
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class TrackingOptionsFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_tracking_options);


            bindPreferenceSummaryToValue(findPreference("enable_tracking"));
            bindPreferenceSummaryToValue(findPreference("tracking_freq"));
            bindPreferenceSummaryToValue(findPreference("tracking_accuracy"));
            bindPreferenceSummaryToValue(findPreference("tracking_timeout"));
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class ImageOptionsFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_image_options);


            bindPreferenceSummaryToValue(findPreference("image_accuracy"));
            bindPreferenceSummaryToValue(findPreference("image_timeout"));
            bindPreferenceSummaryToValue(findPreference("image_delay"));
            bindPreferenceSummaryToValue(findPreference("image_retention"));
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class UploadOptionsFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_upload_options);

            bindPreferenceSummaryToValue(findPreference("upload_type"));
            bindPreferenceSummaryToValue(findPreference("upload_freq"));
            bindPreferenceSummaryToValue(findPreference("upload_size"));
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class OtherOptionsFragment extends PreferenceFragment
    {
        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_other_options);

            bindPreferenceSummaryToValue(findPreference("notifications_enabled"));
            bindPreferenceSummaryToValue(findPreference("notifications_sound"));
        }
    }


}
