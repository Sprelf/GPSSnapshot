package com.sprelf.gpssnapshot.Activities;


import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.sprelf.gpssnapshot.CustomTargets.OffsetViewTarget;
import com.sprelf.gpssnapshot.Fragments.BrowseGroupsFragment;
import com.sprelf.gpssnapshot.Fragments.MyGroupsFragment;
import com.sprelf.gpssnapshot.Fragments.NewGroupFragment;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.LogInResponse;

public class DatabaseBrowserActivity extends Activity implements ActionBar.TabListener
{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_database_browser);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Log in to Parse
        logInUser();

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));



        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++)
        {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                             .setText(mSectionsPagerAdapter.getPageTitle(i))
                             .setTabListener(this));
        }

    }


    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
        //Log.d("[Database Browser]", "Switching to tab " + tab.getPosition() + ", " +
        //                            mSectionsPagerAdapter.getPageTitle(tab.getPosition()));

        forceCloseKeyboard();

        // If the selected tab is the my groups tab, update it
        if (tab.getPosition() == 0)
        {

            Fragment currFrag = mSectionsPagerAdapter.getCurrentPage();

            if (currFrag != null && currFrag instanceof MyGroupsFragment)
            {
                // First time use tutorial is launched from within fragment rather than here.

                ((MyGroupsFragment)currFrag).updateListView();
            }
        } else if (tab.getPosition() == 1)
        {
            Fragment currFrag = mSectionsPagerAdapter.getCurrentPage();
            if (currFrag != null)
            {
                testForFirstTimeUseBrowseGroups(currFrag.getView());

            }
        }
        else if (tab.getPosition() == 2)
        {
            Fragment currFrag = mSectionsPagerAdapter.getCurrentPage();
            if (currFrag != null)
            {
                testForFirstTimeUseNewGroup(currFrag.getView());

            }
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }


    private void forceCloseKeyboard()
    {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null && getWindow().getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
        else
            Log.d("[Keyboard]", "Could not close keyboard.");
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter
    {

        private Fragment currentPage;

        public SectionsPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position)
            {
            case 0:
                return new MyGroupsFragment();
            case 1:
                return new BrowseGroupsFragment();
            case 2:
                return new NewGroupFragment();
            default:
                return null;
            }
        }

        @Override
        public int getCount()
        {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position)
            {
            case 0:
                return getString(R.string.DatabaseBrowser_MyGroupsLabel);
            case 1:
                return getString(R.string.DatabaseBrowser_BrowseGroupsLabel);
            case 2:
                return getString(R.string.DatabaseBrowser_NewGroupLabel);
            }
            return null;
        }

        public Fragment getCurrentPage()
        {
            return currentPage;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object)
        {
            if (object != currentPage)
            {
                currentPage = (Fragment) object;
            }
            super.setPrimaryItem(container, position, object);
        }

    }

    private void logInUser()
    {


        GPSSnapshot.logInUser(this, new LogInResponse()
        {
            @Override
            public void onSuccess()
            {

            }

            @Override
            public void onInvalid()
            {
                GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                GPSSnapshot.ERROR_INVALID_LOGIN, null);
            }

            @Override
            public void onFailure(int errorCode)
            {
                GPSSnapshot.generateErrorDialog(getApplicationContext(),
                                                GPSSnapshot.ERROR_BAD_LOGIN, null);
            }
        });


    }

    public static void testForFirstTimeUseMyGroups(final Activity activity, final View base)
    {
        if (!GPSSnapshot.getBoolPref(activity, "first_my_groups"))
        {
            final OnShowcaseEventListener done = new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    GPSSnapshot.setBoolPref(activity, "first_my_groups", true);
                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };

            OnShowcaseEventListener page1 = new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    new ShowcaseView.Builder(activity)
                            .setTarget(new OffsetViewTarget(base.findViewById(
                                    R.id.DatabaseBrowser_MyGroupsList),
                                                            0.4f, 0.5f))
                            .setContentTitle(R.string.GroupsTutorial_MyGroupsTitle)
                            .setContentText(R.string.GroupsTutorial_MyGroupsText)
                            .hideOnTouchOutside()
                            .setStyle(R.style.DarkTutorial)
                            .setShowcaseEventListener(done)
                            .build();
                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };
            new ShowcaseView.Builder(activity)
                    .setTarget(new OffsetViewTarget(base.findViewById(
                            R.id.DatabaseBrowser_CurrentGroup),
                                                    0.2f, 0.2f))
                    .setContentTitle(R.string.GroupsTutorial_CurrentTitle)
                    .setContentText(R.string.GroupsTutorial_CurrentText)
                    .hideOnTouchOutside()
                    .setStyle(R.style.DarkTutorial)
                    .setShowcaseEventListener(page1)
                    .build();

        }
    }

    private void testForFirstTimeUseBrowseGroups(final View base)
    {
        if (!GPSSnapshot.getBoolPref(this, "first_browse_groups"))
        {
            new ShowcaseView.Builder(this)
                    .setTarget(new OffsetViewTarget(base.findViewById(
                            R.id.DatabaseBrowser_BrowseGroupsFilterArea), 0.25f, 0.8f))
                    .setContentTitle(R.string.GroupsTutorial_BrowseGroupsTitle)
                    .setContentText(R.string.GroupsTutorial_BrowseGroupsText)
                    .hideOnTouchOutside()
                    .setStyle(R.style.DarkTutorial)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView)
                        {
                            GPSSnapshot.setBoolPref(getApplicationContext(), "first_browse_groups",
                                                    true);
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                        {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView)
                        {

                        }
                    })
                    .build();
        }


    }

    private void testForFirstTimeUseNewGroup(final View base)
    {
        if (!GPSSnapshot.getBoolPref(this, "first_new_group"))
        {
            final OnShowcaseEventListener page2 = new OnShowcaseEventListener() {
                @Override
                public void onShowcaseViewHide(ShowcaseView showcaseView)
                {
                    GPSSnapshot.setBoolPref(getApplicationContext(), "first_new_group",
                                            true);
                }

                @Override
                public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                {

                }

                @Override
                public void onShowcaseViewShow(ShowcaseView showcaseView)
                {

                }
            };

            new ShowcaseView.Builder(this)
                    .setTarget(new OffsetViewTarget(base.findViewById(
                            R.id.DatabaseBrowser_NewGroup_CreateButton), 0.5f,0.5f))
                    .setContentTitle(R.string.GroupsTutorial_NewGroupTitle)
                    .setContentText(R.string.GroupsTutorial_NewGroupText)
                    .hideOnTouchOutside()
                    .setStyle(R.style.DarkTutorial)
                    .setShowcaseEventListener(new OnShowcaseEventListener() {
                        @Override
                        public void onShowcaseViewHide(ShowcaseView showcaseView)
                        {
                            new ShowcaseView.Builder(DatabaseBrowserActivity.this)
                                    .setTarget(new OffsetViewTarget(base.findViewById(
                                            R.id.DatabaseBrowser_NewGroup_EnablePublic), 0.3f, 2.5f))
                                    .setContentTitle(R.string.GroupsTutorial_NewGroupSettingsTitle)
                                    .setContentText(R.string.GroupsTutorial_NewGroupSettingsText)
                                    .hideOnTouchOutside()
                                    .setStyle(R.style.DarkTutorial)
                                    .setShowcaseEventListener(page2)
                                    .build();
                        }

                        @Override
                        public void onShowcaseViewDidHide(ShowcaseView showcaseView)
                        {

                        }

                        @Override
                        public void onShowcaseViewShow(ShowcaseView showcaseView)
                        {

                        }
                    })
                    .build();
        }


    }


}
