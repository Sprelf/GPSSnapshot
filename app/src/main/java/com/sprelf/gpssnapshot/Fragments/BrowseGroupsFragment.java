package com.sprelf.gpssnapshot.Fragments;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.sprelf.gpssnapshot.Adapters.GroupListAdapter;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment for browsing all public groups.
 */
public class BrowseGroupsFragment extends Fragment
{

    private ArrayList<DataGroup> groupList, displayList;
    private ListView listView;
    private EditText filterView;

    public BrowseGroupsFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_browse_groups, container, false);

        groupList = new ArrayList<>();
        displayList = new ArrayList<>();
        setUpListView(rootView);
        setUpFilterBox(rootView);

        return rootView;
    }

    private void setUpListView(View rootView)
    {
        listView = (ListView) rootView.findViewById(R.id.DatabaseBrowser_BrowseGroupsList);
        listView.setAdapter(new GroupListAdapter(getActivity(), displayList, true));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view,
                                    final int position, long id)
            {
                filterView.clearFocus();
                if (displayList != null)
                {
                    // if the group is not already in My Groups
                    if (!GPSSnapshot.isInMyGroups(getActivity(), displayList.get(position)))
                    {
                        // if there is no password
                        if (displayList.get(position).accessCode == null ||
                            displayList.get(position).accessCode.equals(""))
                        {
                            // Add group the My Groups database.  If it is new, play animation
                            addGroup(position, view);
                        }
                        else // if there is a password
                        {
                            // Create a password dialog box
                            createPasswordDialog(position, view);
                        }
                    }
                }
            }
        });
        getParseGroups(listView);
    }

    private void addGroup(final int position, final View view)
    {
        GPSSnapshot.addGroup(getActivity(), displayList.get(position));

        new Handler().post(new Runnable()
        {
            @Override
            public void run()
            {
                playSlidingAnimation(position, view);
            }
        });

    }

    private void createPasswordDialog(final int position, final View view)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View dialogView = View.inflate(getActivity(), R.layout.layout_password_dialog, null);
        builder.setMessage(R.string.DatabaseBrowser_PasswordDialog_Message)
               .setView(dialogView)
               .setPositiveButton(R.string.DatabaseBrowser_PasswordDialog_Submit,
                                  new DialogInterface.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which)
                                      {
                                          String text = ((EditText) dialogView.findViewById(
                                                  R.id.PasswordDialog_Input)).getText().toString();
                                          if (text.equals(displayList.get(position).accessCode))
                                              addGroup(position, view);
                                          else
                                              Toast.makeText(
                                                      getActivity(),
                                                      R.string.DatabaseBrowser_PasswordDialog_Failure,
                                                      Toast.LENGTH_SHORT).show();
                                      }
                                  });
        builder.show();
    }


    private void playSlidingAnimation(int position, View view)
    {
        DataGroup data = displayList.get(position);

        final View animView = View.inflate(getActivity(), R.layout.layout_group_info, null);
        final ViewGroup animContainer = ((ViewGroup) getActivity()
                .findViewById(R.id.DatabaseBrowser_BrowseGroupsAnimArea));
        animContainer.addView(animView);

        GroupListAdapter.attachGroupValsToView(animView, data);
        float startX = view.getTranslationX();
        float startY = view.getY()+(float)getActivity().findViewById(
                R.id.DatabaseBrowser_BrowseGroupsFilterArea).getHeight();
        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        float endX = -size.x;
        float endY = startY + 40;
        int xDuration = 400;
        int yDuration = 100;

        animContainer.setTranslationX(startX);

        ObjectAnimator xAnim = ObjectAnimator.ofFloat(animContainer, "translationX", startX, endX);
        xAnim.setDuration(xDuration);
        xAnim.setInterpolator(new AccelerateDecelerateInterpolator());
        ObjectAnimator yAnim = ObjectAnimator.ofFloat(animContainer, "translationY", startY, endY);
        yAnim.setDuration(yDuration);

        AnimatorSet animSet = new AnimatorSet();
        animSet.addListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animation)
            {

            }

            @Override
            public void onAnimationEnd(Animator animation)
            {
                animContainer.removeView(animView);
            }

            @Override
            public void onAnimationCancel(Animator animation)
            {
                animContainer.removeView(animView);
            }

            @Override
            public void onAnimationRepeat(Animator animation)
            {

            }
        });
        animSet.play(xAnim).after(yAnim);
        animSet.start();

    }

    private void setUpFilterBox(View rootView)
    {
        filterView = ((EditText) rootView.findViewById(R.id.DatabaseBrowser_BrowseGroupsFilter));
        filterView.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

            }

            @Override
            public void afterTextChanged(Editable s)
            {
                String filter = s.toString().toLowerCase();
                displayList.clear();
                for (DataGroup group : groupList)
                {
                    if (group.title.toLowerCase().contains(filter)
                        || group.description.toLowerCase().contains(filter))
                        displayList.add(group);
                }
                new Handler().post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        ((GroupListAdapter) listView.getAdapter()).notifyDataSetChanged();
                    }
                });
            }
        });
    }

    private void getParseGroups(final ListView listView)
    {
        // Check for possible errors.
        // Internet connectivity error:
        if (!GPSSnapshot.isInternetConnected(getActivity()))
        {
            GPSSnapshot.generateErrorDialog(getActivity(), GPSSnapshot.ERROR_NO_INTERNET, null);
            return;
        }


        ParseQuery<ParseObject> query = ParseQuery.getQuery("DataGroup");
        Log.d("[Database Browser]", "Querying parse for list of databases...");
        query.findInBackground(new FindCallback<ParseObject>()
        {
            @Override
            public void done(List<ParseObject> list, ParseException e)
            {
                if (e != null)
                {
                    Log.e("[Database Browser]", "Encountered error retrieving databases.\n" +
                                                e.getMessage() + "\nError Code = " + e.getCode());
                    e.printStackTrace();
                    GPSSnapshot.generateErrorDialog(getActivity(), GPSSnapshot.ERROR_PARSE, null);
                }
                else
                {
                    Log.d("[Database Browser]", "Database list retrieval successful.  Returned "
                                                + list.size() + " results.");
                    for (ParseObject obj : list)
                    {
                        groupList.add(new DataGroup(obj));
                    }
                    displayList.addAll(groupList);

                    new Handler().post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            ((GroupListAdapter) listView.getAdapter()).notifyDataSetChanged();
                        }
                    });

                }
            }
        });

    }
}
