package com.sprelf.gpssnapshot.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.sprelf.gpssnapshot.Activities.DatabaseBrowserActivity;
import com.sprelf.gpssnapshot.Adapters.GroupListAdapter;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.Handlers.DatabaseHandler;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment for displaying the user's groups.
 */
public class MyGroupsFragment extends Fragment
{

    RelativeLayout currentGroupView;
    ListView listView;
    ArrayList<DataGroup> objects;
    DataGroup currentGroup;
    DatabaseHandler mDbHelper;
    SQLiteDatabase mDb;

    public MyGroupsFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mDbHelper = new DatabaseHandler(getActivity());
        mDb = mDbHelper.getWritableDatabase();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_my_groups, container, false);

        // Reference the location where the current group view is to be
        currentGroupView = (RelativeLayout)rootView.findViewById(R.id.DatabaseBrowser_CurrentGroup);
        // Inflate a group info window into this view
        currentGroupView.addView(inflater.inflate(
                R.layout.layout_group_info, currentGroupView, false));
        // Retrieve the current group
        currentGroup = GPSSnapshot.getCurrentGroup(getActivity());
        // Modify the values of the current group view based on the current group
        updateCurrentGroupView();
        // Add the onLongClick listeners to the current group view with the associated dialogs
        setCurrentGroupTouchListener();

        // Reference the list view in the fragment
        listView = (ListView) rootView.findViewById(R.id.DatabaseBrowser_MyGroupsList);
        // Attach the adapter and onItemClickListener
        setUpListView();

        new Handler().post(new Runnable()
        {
            @Override
            public void run()
            {
                DatabaseBrowserActivity.testForFirstTimeUseMyGroups(getActivity(), rootView);
            }
        });


        return rootView;
    }

    /** Retrieves all data groups from the SQL database, and stores them in the Array List
     *  'objects'.  If objects is null when this is called, objects is instantiated.  If not,
     *  then objects is cleared before adding all the entries.  In this way, the method serves
     *  its role both as a builder and as an updater for the My Groups list.
     */

    private void getLocalGroups()
    {
        // If objects has not been instantiated, do so.  If not, simply clear it so that its
        //  reference does not change and the GroupListAdapter can be easily updated.
        if (objects==null)
            objects = new ArrayList<>();
        else
            objects.clear();

        objects.addAll(GPSSnapshot.getMyGroups(getActivity()));

    }

    /** Updates the current group view region with information held in the DataGroup 'currentGroup'.
     * If currentGroup is null, an error is displayed in its place.
     */
    private void updateCurrentGroupView()
    {
        // Find all views to modify in current group view
        TextView groupNameText = (TextView) currentGroupView.findViewById(
                R.id.GroupInfo_GroupTitle);
        TextView groupDescriptionText = (TextView) currentGroupView.findViewById(
                R.id.GroupInfo_GroupDescription);
        TextView groupAdminText = (TextView) currentGroupView.findViewById(
                R.id.GroupInfo_AdminBox);
        View baseView = currentGroupView.findViewById(R.id.GroupInfo_BaseView);

        // Set view based on group data
        if (currentGroup!=null)
        {
            changeViewBackground(baseView, R.drawable.group_info_background_current);

            groupNameText.setText(currentGroup.title);
            groupDescriptionText.setText(currentGroup.description);
            if (currentGroup.isAdmin)
                groupAdminText.setVisibility(View.VISIBLE);
        }
        else
        {
            groupNameText.setText(R.string.CurrGroup_DefaultName);
            groupDescriptionText.setText(R.string.CurrGroup_DefaultDescription);
            groupAdminText.setVisibility(View.INVISIBLE);

            changeViewBackground(baseView, R.drawable.group_info_background_null);
        }
    }


    /** Removes from the data group held in 'currentGroup' from currentGroup, from the My Groups
     * array, and from the local database.
     */
    private void deleteCurrentGroup()
    {
        Log.d("[Database Browser]", "Deleting group "+currentGroup.title+" from My Groups.");

        // Remove from list of my groups
        for (DataGroup obj : objects)
        {
            if (obj.title.equals(currentGroup.title))
            {
                objects.remove(obj);
                break;
            }
        }
        ((GroupListAdapter) listView.getAdapter()).notifyDataSetChanged();

        // Delete entry in database
        mDb.delete(DatabaseHandler.GROUP_TABLE_NAME,
                   DatabaseHandler.GROUP_TITLE + " = '" + currentGroup.title + "'",
                   null);

        // Nullify current group
        currentGroup = null;

        // Eliminate the saved current group settings
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = settings.edit();

        editor.putString("currgroup_name", "");
        editor.putString("currgroup_description", "");
        editor.putString("currgroup_access_code", "");
        editor.putString("currgroup_app_id", "");
        editor.putString("currgroup_client_key", "");
        editor.putBoolean("currgroup_is_admin", false);

        editor.commit();

        // Update the current group view
        updateCurrentGroupView();
    }

    private void modifyCurrentGroup()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final View dialogView = View.inflate(getActivity(),
                                             R.layout.layout_modify_group_dialog, null);
        DataGroup currGroup = GPSSnapshot.getCurrentGroup(getActivity());
        ((CheckBox) dialogView.findViewById(R.id.ModifyGroupDialog_EnableUrgentOnly))
                .setChecked(currGroup.notifyUrgentOnly);
        ((CheckBox) dialogView.findViewById(R.id.ModifyGroupDialog_EnablePublic))
                .setChecked(currGroup.publicEntries);

        builder.setMessage(R.string.DatabaseBrowser_ModifyGroupDialog_Message)
               .setView(dialogView)
               .setPositiveButton(R.string.DatabaseBrowser_PasswordDialog_Submit,
                                  new DialogInterface.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(DialogInterface dialog, int which)
                                      {
                                          String text = ((EditText) dialogView.findViewById(
                                                  R.id.ModifyGroupDialog_DescriptionInput))
                                                  .getText().toString();
                                          boolean enablePublic = ((CheckBox) dialogView.findViewById(
                                              R.id.ModifyGroupDialog_EnablePublic)).isChecked();
                                          boolean notify = ((CheckBox) dialogView.findViewById(
                                                  R.id.ModifyGroupDialog_EnableUrgentOnly))
                                                  .isChecked();
                                          updateGroupSettings(text, enablePublic, notify);
                                      }
                                  });
        builder.show();
    }

    private void updateGroupSettings(final String text, final boolean publicEntries,
                                     final boolean notifyUrgentOnly)
    {
        Log.d("[Database Browser]", "Updating settings of group '"+
             GPSSnapshot.getStringPref(getActivity(), "currgroup_name")+"'...");

        ParseQuery<ParseObject> query = ParseQuery.getQuery("DataGroup");
        query.whereEqualTo("group_name", GPSSnapshot.getStringPref(getActivity(), "currgroup_name"));
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e)
            {
                if (e!=null)
                {
                    Log.e("[Database Browser]", "Error querying Parse.\n"+e.getMessage()+
                         "\nError Code = "+e.getCode());
                    e.printStackTrace();
                }
                else
                {
                    if (list.size()>0)
                    {
                        ParseObject obj = list.get(0);
                        obj.put("description", text);
                        obj.put("notify_urgent_only", notifyUrgentOnly);
                        obj.put("public_entries", publicEntries);
                        obj.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e)
                            {
                                if (e!=null)
                                {
                                    Log.e("[Database Browser]", "Error querying Parse.\n"
                                                                +e.getMessage()+
                                                                "\nError Code = "+e.getCode());
                                    e.printStackTrace();
                                }
                                else
                                {
                                    Log.d("[Database Browser]", "Successfully updated group.");
                                    DataGroup group = GPSSnapshot.getCurrentGroup(getActivity());
                                    if (group==null)
                                        return;
                                    group.description = text;
                                    group.publicEntries = publicEntries;
                                    group.notifyUrgentOnly = notifyUrgentOnly;
                                    GPSSnapshot.addGroup(getActivity(), group);
                                    GPSSnapshot.setCurrentGroup(getActivity(), group);
                                    updateListView();
                                    updateCurrentGroupView();
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    /** Calls for an update of the 'objects' list, attaches the GroupListAdapter, and adds the
     * OnItemClickListener.
     */
    private void setUpListView()
    {
        // Update the My Groups list
        getLocalGroups();
        // Attach the adapter
        listView.setAdapter(new GroupListAdapter(getActivity(), objects, false));
        // Apply the OnItemClickListener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                // Update the current group, save it, and update the current group view.
                currentGroup = objects.get(position);
                GPSSnapshot.setCurrentGroup(getActivity(), currentGroup);
                updateCurrentGroupView();
            }
        });
    }

    /** Updates the 'objects' list based on the SQL database.  Generally called when tabbing back
     * to this fragment, to ensure all modifications are recognized.
     */
    public void updateListView()
    {
        if (objects!=null)
        {
            Log.d("[Database Browser]", "Updating My Groups list.");
            getLocalGroups();
            ((GroupListAdapter) listView.getAdapter()).notifyDataSetChanged();
        }
    }

    /** Creates the dialog boxes used for deleting and modifying the current group, and attaches
     * them to the current group view with an OnLongClickListener.
     */
    private void setCurrentGroupTouchListener()
    {
        // Dialog window that pops up if the user is the admin of the current group
        final AlertDialog.Builder adminBuilder = new AlertDialog.Builder(getActivity());
        adminBuilder.setMessage(R.string.DatabaseBrowser_AdminDialog_Message)
                // Cancel Button
                    .setNegativeButton(R.string.DatabaseBrowser_AdminDialog_Cancel,
                                       new DialogInterface.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(DialogInterface dialog, int which)
                                           {
                                               // DO NOTHING
                                           }
                                       })
                // Modify Button
                    .setNeutralButton(R.string.DatabaseBrowser_AdminDialog_Modify,
                                      new DialogInterface.OnClickListener()
                                      {
                                          @Override
                                          public void onClick(DialogInterface dialog, int which)
                                          {
                                              modifyCurrentGroup();
                                          }
                                      })
                // Delete Button
                    .setPositiveButton(R.string.DatabaseBrowser_AdminDialog_Delete,
                                       new DialogInterface.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(DialogInterface dialog, int which)
                                           {
                                               deleteCurrentGroup();
                                           }
                                       });

        // Dialog that pops up if user is not the admin of the current group
        final AlertDialog.Builder nonAdminBuilder = new AlertDialog.Builder(getActivity());
        nonAdminBuilder.setMessage(R.string.DatabaseBrowser_NonAdminDialog_Message)
                // Cancel Button
                    .setNegativeButton(R.string.DatabaseBrowser_NonAdminDialog_Cancel,
                                       new DialogInterface.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(DialogInterface dialog, int which)
                                           {
                                                // DO NOTHING
                                           }
                                       })
                // Delete Button
                    .setPositiveButton(R.string.DatabaseBrowser_NonAdminDialog_Confirm,
                                       new DialogInterface.OnClickListener()
                                       {
                                           @Override
                                           public void onClick(DialogInterface dialog, int which)
                                           {
                                               deleteCurrentGroup();
                                           }
                                       });


        // Add the long click listener to the current group view
        currentGroupView.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                if (currentGroup==null);
                else if (currentGroup.isAdmin)
                    adminBuilder.create().show();
                else
                    nonAdminBuilder.create().show();

                return true;
            }
        });
    }

    /**
     * Sets the background of the given view to the given drawable resource ID.  For some reason,
     * changing the background would destroy the padding, so I had to implement this as a
     * workaround.
     *
     * @param view     View whose background to change
     * @param drawable ID of drawable resource to use
     */
    private void changeViewBackground(View view, int drawable)
    {
        int pL = view.getPaddingLeft();
        int pT = view.getPaddingTop();
        int pR = view.getPaddingRight();
        int pB = view.getPaddingBottom();

        view.setBackgroundResource(drawable);

        view.setPadding(pL, pT, pR, pB);
    }
}