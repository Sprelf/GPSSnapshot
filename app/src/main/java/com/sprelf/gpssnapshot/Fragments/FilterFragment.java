package com.sprelf.gpssnapshot.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sprelf.gpssnapshot.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFilterChangeListener} interface
 * to handle interaction events.
 */
public class FilterFragment extends Fragment
{

    private OnFilterChangeListener mListener;

    public FilterFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_filter, container, false);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        mListener.onFragmentLayoutComplete();
    }


    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFilterChangeListener) activity;
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                                         + " must implement OnFilterChangeListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    public interface OnFilterChangeListener
    {
        public void onFragmentLayoutComplete();
    }

}
