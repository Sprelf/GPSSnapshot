package com.sprelf.gpssnapshot.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;

import java.util.List;

/**
 * A fragment for creating a new group
 */
public class NewGroupFragment extends Fragment
{

    public NewGroupFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_new_group, container, false);

        setUpCreateButton(rootView);

        return rootView;
    }

    /** Attaches On Click Listener to the create button that handles group creation, including
     * checking for uniqueness and saving to Parse database.
     *
     * @param rootView View containing all elements
     */
    private void setUpCreateButton(View rootView)
    {


        final EditText nameText = (EditText)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_NameInput);
        final EditText descText = (EditText)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_DescriptionInput);
        final EditText passwordText = (EditText)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_PasswordInput);
        final TextView errorText = (TextView)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_ErrorText);
        final CheckBox publicCheck = (CheckBox)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_EnablePublic);
        final CheckBox urgentOnly = (CheckBox)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_EnableUrgentOnly);
        Button createButton = (Button)rootView
                .findViewById(R.id.DatabaseBrowser_NewGroup_CreateButton);



        createButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                final String name = nameText.getText().toString();
                final String desc = descText.getText().toString();
                final String pass = passwordText.getText().toString();


                Log.d("[Database Browser]", "Creating group '"+name+"'...");

                // Test for internet connectivity:
                if (!GPSSnapshot.isInternetConnected(getActivity()))
                {
                    GPSSnapshot.generateErrorDialog(getActivity(),
                                                    GPSSnapshot.ERROR_NO_INTERNET, null);
                }
                else if (!ParseUser.getCurrentUser().isAuthenticated())
                {
                    GPSSnapshot.generateErrorDialog(getActivity(),
                                                    GPSSnapshot.ERROR_NO_INTERNET, null);
                }
                // Test that the name is valid:
                else if (!GPSSnapshot.isValidName(name))
                {
                    GPSSnapshot.generateErrorDialog(getActivity(),
                                                    GPSSnapshot.ERROR_INVALID_NAME, null);
                    errorText.setText(R.string.DatabaseBrowser_NewGroup_NameInvalidMessage);
                    errorText.setVisibility(View.VISIBLE);
                }
                // Test that the name is not reserved:
                else if (name.equals("Public"))
                {
                    GPSSnapshot.generateErrorDialog(getActivity(),
                                                    GPSSnapshot.ERROR_RESERVED_NAME, null);
                    errorText.setText(R.string.DatabaseBrowser_NewGroup_NameReservedMessage);
                    errorText.setVisibility(View.VISIBLE);
                }
                else
                {

                    // Test if group name already exists
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("DataGroup");
                    query.whereEqualTo("group_name", name);
                    query.findInBackground(new FindCallback<ParseObject>()
                    {
                        @Override
                        public void done(List<ParseObject> list, ParseException e)
                        {
                            if (e != null)
                            {
                                Log.e("[Database Browser]", "Error creating new group.\n"
                                                            + e.getMessage()
                                                            + "\nError Code = " + e.getCode());
                                e.printStackTrace();
                                GPSSnapshot.generateErrorDialog(
                                        getActivity(), GPSSnapshot.ERROR_PARSE, null);
                                return;
                            }

                            // If the query returns any results, quit creating group
                            if (list.size() > 0)
                            {
                                Log.d("[Database Browser]",
                                      "Group name already exists.  Aborting.");
                                errorText.setText(
                                        R.string.DatabaseBrowser_NewGroup_NameTakenMessage);
                                errorText.setVisibility(View.VISIBLE);
                                GPSSnapshot.generateErrorDialog(getActivity(),
                                                                GPSSnapshot.ERROR_TAKEN_NAME, null);
                                return;
                            }

                            if (errorText.getVisibility() == View.VISIBLE)
                                errorText.setVisibility(View.INVISIBLE);
                            nameText.setText("");
                            descText.setText("");
                            passwordText.setText("");

                            final DataGroup newGroup = new DataGroup();
                            newGroup.title = name;
                            newGroup.description = desc;
                            newGroup.accessCode = pass;
                            newGroup.isAdmin = true;
                            newGroup.appId = GPSSnapshot.PARSE_APPID;
                            newGroup.clientKey = GPSSnapshot.PARSE_CLIENTKEY;
                            newGroup.notifyUrgentOnly = urgentOnly.isChecked();
                            newGroup.publicEntries = publicCheck.isChecked();

                            ParseObject newGroupObj = newGroup.asParseObject(ParseUser.getCurrentUser());
                            newGroupObj.saveInBackground(new SaveCallback()
                            {
                                @Override
                                public void done(ParseException e)
                                {
                                    final String toastMessage;
                                    if (e != null)
                                    {
                                        Log.e("[Database Browser]", "Error creating new group.\n"
                                                                    + e.getMessage()
                                                                    + "\nError Code = " + e.getCode());
                                        e.printStackTrace();
                                        toastMessage = getResources().getString(
                                                R.string.DatabaseBrowser_NewGroup_ErrorMessage);
                                        GPSSnapshot.generateErrorDialog(getActivity(),
                                                                        GPSSnapshot.ERROR_PARSE,
                                                                        null);
                                    }
                                    else
                                    {
                                        Log.d("[Database Browser]", "Successfully created group.");
                                        toastMessage = getResources().getString(
                                                R.string.DatabaseBrowser_NewGroup_SuccessMessage);
                                        GPSSnapshot.addGroup(getActivity(), newGroup);
                                        GPSSnapshot.setCurrentGroup(getActivity(), newGroup);
                                    }
                                    new Handler().post(new Runnable()
                                    {
                                        @Override
                                        public void run()
                                        {
                                            Toast.makeText(getActivity(), toastMessage,
                                                           Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            });
                        }
                    });

                }

            }
        });



    }
}