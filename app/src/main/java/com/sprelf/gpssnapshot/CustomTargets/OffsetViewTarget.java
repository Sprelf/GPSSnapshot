package com.sprelf.gpssnapshot.CustomTargets;

import android.app.Activity;
import android.graphics.Point;
import android.view.View;

import com.github.amlcurran.showcaseview.targets.Target;

/**
 * Created by Chris on 29.10.2015.
 */
public class OffsetViewTarget implements Target
{

    float xOffset, yOffset;
    private final View mView;

    public OffsetViewTarget(View view, float xOffset, float yOffset)
    {
        mView = view;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    public OffsetViewTarget(int viewId, Activity activity, float xOffset, float yOffset) {
        mView = activity.findViewById(viewId);
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    @Override
    public Point getPoint() {
        int[] location = new int[2];
        mView.getLocationInWindow(location);
        int x = (int)(location[0] + mView.getWidth() * xOffset);
        int y = (int)(location[1] + mView.getHeight() * yOffset);
        return new Point(x, y);
    }
}
