package com.sprelf.gpssnapshot.Receivers;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sprelf.gpssnapshot.Handlers.DatabaseHandler;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.LogInResponse;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class UploadReceiver extends BroadcastReceiver
{
    public static String TIME_KEY = "time";
    public static String PIC_KEY = "image";
    public static String GEOPOINT_KEY = "geopoint";
    public static String USER_KEY = "user";
    public static String USER_NAME_KEY = "display_name";
    public static String USER_EMAIL_KEY = "user_email";
    public static String USER_PHONE_KEY = "phone_number";
    public static String GROUP_NAME_KEY = "group_name";
    public static String DESCRIPTION_KEY = "description";
    public static String URGENT_KEY = "urgent";

    public static int UPLOAD_IMAGE_MAX_WIDTH = 800;  // in pixels
    public static int UPLOAD_IMAGE_MAX_HEIGHT = 450; // in pixels
    public static int UPLOAD_IMAGE_MAX_SIZE = 45000;  // in bytes

    public static final String ACTION_FORCE_UPLOAD =
            "com.sprelf.gpssnapshot.UploadReceiver.ACTION_FORCE_UPLOAD";

    private ParseUser user;
    private SQLiteDatabase mDb;
    private DatabaseHandler mDbHelper;

    Cursor dataCursor, trackCursor;

    private Context c;

    public UploadReceiver()
    {
    }

    @Override
    public void onReceive(Context cont, Intent intent)
    {

        this.c = cont;

        // Initialize database access
        mDbHelper = new DatabaseHandler(c);
        mDb = mDbHelper.getWritableDatabase();

        // If we are connected to internet, do all uploading.  Otherwise do nothing.
        if (canConnect())
        {
            Log.d("[Upload]", "Internet detected.");

            (new AsyncTask<String, String, String>()
            {
                @Override
                protected String doInBackground(String... arg0)
                {

                    // Retrieve all database elements that need to be submitted
                    dataCursor = mDb.query(DatabaseHandler.TABLE_NAME, DatabaseHandler.COLUMNS,
                                           DatabaseHandler.SUBMITTED + " = '0'",
                                           null, null, null, null);
                    trackCursor = mDb.query(DatabaseHandler.TRACK_TABLE_NAME,
                                            DatabaseHandler.TRACK_COLUMNS,
                                            null, null, null, null, null);

                    // If there are pending submissions, submit them.
                    if (dataCursor.getCount() > 0 || trackCursor.getCount() > 0)
                    {
                        Log.d("[Upload]", "Found " + dataCursor.getCount() + " data entries"
                                          + " and " + trackCursor.getCount() + " tracking entries to upload.");
                        performNetworkActivities();
                    }
                    else
                    {
                        Log.d("[Upload]", "No entries to upload.");
                    }
                    return null;
                }
            }).execute();
        }

    }

    /**
     * Determines whether or not the uploader has the ability and permission to perform upload.
     *
     * @return Returns true if upload may proceed, false otherwise
     */
    private boolean canConnect()
    {
        NetworkInfo info = ((ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();

        if (info == null || !info.isConnected())
        {
            Log.d("[Upload]", "Upload failed, no internet connection detected.");
            return false;
        }


        if (GPSSnapshot.getStringPref(c, "upload_type")
                       .equals(c.getString(R.string.pref_3g_or_wifi))
            && info.isConnected())
        {
            return true;
        }

        if (GPSSnapshot.getStringPref(c, "upload_type")
                       .equals(c.getString(R.string.pref_wifi_only))
            && info.isConnected()
            && info.getType() == ConnectivityManager.TYPE_WIFI)
        {
            return true;
        }

        if (GPSSnapshot.getStringPref(c, "upload_type")
                       .equals(c.getString(R.string.pref_never)))
        {
            Log.d("[Upload]", "Uploading currently disabled.");
        }

        Log.d("[Upload]", "Upload conditions are not met, aborting upload.");
        return false;
    }

    /**
     * Handles uploading of all data based by the Cursor to the Parse database.
     */
    private void performNetworkActivities()
    {
        GPSSnapshot.logInUser(c, new LogInResponse()
        {
            @Override
            public void onSuccess()
            {
                uploadData();
            }

            @Override
            public void onInvalid()
            {
                GPSSnapshot.generateLoginErrorNotification(c);
            }

            @Override
            public void onFailure(int errorCode)
            {
                GPSSnapshot.generateLoginErrorNotification(c);
            }
        });
    }

    /**
     * Handles the construction of all data held in dataCursor and trackCursor into ParseObjects
     * and uploads that data.
     */
    private void uploadData()
    {


        user = ParseUser.getCurrentUser();
        if (user.get(USER_NAME_KEY) != null &&
            !user.get(USER_NAME_KEY).equals(GPSSnapshot.getStringPref(c, "display_name"))) ;
        {
            user.put(USER_NAME_KEY, GPSSnapshot.getStringPref(c, "display_name"));
            user.saveEventually();
        }
        if (user.get(USER_PHONE_KEY) != null &&
                !user.get(USER_PHONE_KEY).equals(GPSSnapshot.getStringPref(c, "user_phone")))
        {
            user.put(USER_PHONE_KEY, GPSSnapshot.getStringPref(c, "user_phone"));
            user.saveEventually();
        }


        // Iterate through all results
        while (dataCursor.moveToNext())
        {
            final ParseObject object = new ParseObject("DataPoint");

            final String picPath = dataCursor.getString(dataCursor.getColumnIndex(DatabaseHandler.PIC_PATH));
            String time = dataCursor.getString(dataCursor.getColumnIndex(DatabaseHandler.TIME));
            float lati = dataCursor.getFloat(dataCursor.getColumnIndex(DatabaseHandler.LATITUDE));
            float longi = dataCursor.getFloat(dataCursor.getColumnIndex(DatabaseHandler.LONGITUDE));
            String desc = dataCursor.getString(dataCursor.getColumnIndex(DatabaseHandler.DESCRIPTION));
            String group = dataCursor.getString(dataCursor.getColumnIndex(DatabaseHandler.GROUP_NAME));
            boolean isUrgent = (dataCursor.getInt(
                    dataCursor.getColumnIndex(DatabaseHandler.IS_URGENT)) == 1);
            ParseGeoPoint geoPoint = new ParseGeoPoint(lati, longi);


            addBasicData(object, time, geoPoint);

            // Add description and group
            object.put(DESCRIPTION_KEY, desc);
            object.put(GROUP_NAME_KEY, group);
            object.put(URGENT_KEY, isUrgent);
            object.put(USER_PHONE_KEY, GPSSnapshot.getStringPref(c, "user_phone"));

            // Load the image, convert it to PNG, and store
            //  the byte array.
            try
            {
                // Open the base image.
                File file = new File(picPath);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(file), null, options);

                // Resize the image as needed to fit in require width
                int sampleSize = options.outWidth / UPLOAD_IMAGE_MAX_WIDTH;
                if (sampleSize < 1) { sampleSize = 1; }
                options.inSampleSize = sampleSize;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inJustDecodeBounds = false;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options);

                // Find the maximum quality that will fit inside the maximum file size by trying
                //  lower and lower quality until it fits.
                int compressStep = 10;  // Larger amount means faster, but less precise
                int compressQuality = 100 + compressStep;
                int streamLength = GPSSnapshot.getIntPref(c, "upload_size") * 1024;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                while (streamLength >= GPSSnapshot.getIntPref(c, "upload_size") * 1024
                       && compressQuality > compressStep)
                {
                    try
                    {
                        stream.flush();
                        stream.reset();
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    compressQuality -= compressStep;
                    bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, stream);
                    byte[] byteArray = stream.toByteArray();
                    streamLength = byteArray.length;
                }

                if (compressQuality > 0)
                {
                    Log.d("[Image Rescaling]", "Image scaled to " + streamLength + " bytes, " +
                                               "at quality " + compressQuality);
                    final ParseFile parseFile = new ParseFile(time + ".jpg", stream.toByteArray());
                    parseFile.saveInBackground(new SaveCallback()
                    {
                        @Override
                        public void done(com.parse.ParseException e)
                        {
                            if (e != null)
                            {
                                Log.e("[Upload]", "Error uploading image to Parse.\n"
                                                  + e.getMessage());
                                e.printStackTrace();
                            }
                            else
                            {
                                object.put(PIC_KEY, parseFile);
                                object.saveInBackground();
                                Log.d("[Upload]", "Image file successfully uploaded - " + picPath);
                            }
                        }
                    });
                }
                else
                {
                    Log.d("[Image Rescaling]", "Unable to scale image to appropriate size.");
                }

                // Save the object, and if successful, report success and mark data point as submitted.
                object.saveInBackground(new SaveCallback()
                {
                    @Override
                    public void done(com.parse.ParseException e)
                    {
                        if (e != null)
                        {
                            Log.e("[Upload]", "Encountered error uploading data with Parse.\n" +
                                              e.getMessage());
                            e.printStackTrace();
                        }
                        else
                        {
                            // Mark entry in database as having been submitted, and attempt to
                            ContentValues val = new ContentValues();
                            val.put(DatabaseHandler.SUBMITTED, 1);
                            mDb.update(DatabaseHandler.TABLE_NAME,
                                       val,
                                       DatabaseHandler.PIC_PATH + " = '" + picPath + "'",
                                       null);
                            Log.d("[Upload]", "Data point upload successful - " + picPath);
                        }
                    }
                });

            } catch (IOException e)
            {
                Log.e("[Data]", e.getMessage());
            }
        }

        // If image data was submitted, attempt to clean picture folder in case the current setting
        // is to delete upon upload.
        if (dataCursor.getCount() > 0)
            GPSSnapshot.cleanPicturesFolder(c);

        while (trackCursor.moveToNext())
        {
            ParseObject object = new ParseObject("TrackingPoint");

            final String time = trackCursor.getString(trackCursor.getColumnIndex(
                    DatabaseHandler.TRACK_TIME));
            float lati = trackCursor.getFloat(trackCursor.getColumnIndex(
                    DatabaseHandler.TRACK_LATITUDE));
            float longi = trackCursor.getFloat(trackCursor.getColumnIndex(
                    DatabaseHandler.TRACK_LONGITUDE));
            ParseGeoPoint geoPoint = new ParseGeoPoint(lati, longi);

            // Add time, GPS, and user data to the object
            addBasicData(object, time, geoPoint);

            // Add current group
            object.put(GROUP_NAME_KEY, GPSSnapshot.getStringPref(c, "currgroup_name"));

            // Save the object, and if successful, report success and delete tracking entry.
            object.saveInBackground(new SaveCallback()
            {
                @Override
                public void done(com.parse.ParseException e)
                {
                    if (e != null)
                    {
                        Log.e("[Upload]", "Upload of tracking data failed.\n" +
                                          e.getMessage());
                        e.printStackTrace();
                    }
                    else
                    {
                        // Delete tracking entry in database
                        mDb.delete(DatabaseHandler.TRACK_TABLE_NAME,
                                   DatabaseHandler.TRACK_TIME + " = " + time,
                                   null);

                        Log.d("[Upload]", "Upload of tracking data successful.");
                    }
                }
            });
        }


    }

    /**
     * Adds basic data into the given Parse Object.  Basic data includes the given time, the given
     * geopoint, as well as all user information associated with the current user.
     *
     * @param object   Parse Object to put data into
     * @param time     String representing the time to put into the given object
     * @param geoPoint Geopoint to put into the given object
     */
    private void addBasicData(ParseObject object, String time, ParseGeoPoint geoPoint)
    {
        // Parse date from string and add it
        try
        {
            Date date = GPSSnapshot.DATE_FORMAT.parse(time);

            object.put(TIME_KEY, date);
        } catch (ParseException e)
        {
            Log.e("[Data]", e.getMessage());
            e.printStackTrace();
        }

        // Add Lat/Long Coords
        object.put(GEOPOINT_KEY, geoPoint);

        // Add user information
        object.put(USER_KEY, user);
        object.put(USER_NAME_KEY, GPSSnapshot.getStringPref(c, "display_name"));
        if (user.has("email"))
            object.put(USER_EMAIL_KEY, user.getEmail());


    }


}

