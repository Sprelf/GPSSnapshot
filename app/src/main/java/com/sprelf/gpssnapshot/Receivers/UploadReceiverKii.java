
// Code no longer used, but wanted to keep it around in case I wanted to migrate back to Kii.
// Code is severely out of date, serves merely as a reference.

/*package com.sprelf.gpssnapshot;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kii.cloud.storage.GeoPoint;
import com.kii.cloud.storage.Kii;
import com.kii.cloud.storage.KiiObject;
import com.kii.cloud.storage.KiiUser;
import com.kii.cloud.storage.UserFields;
import com.kii.cloud.storage.callback.KiiObjectCallBack;
import com.kii.cloud.storage.exception.app.AppException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;

public class UploadReceiverKii extends BroadcastReceiver
{

    public static String TIME_KEY = "time";
    public static String PIC_KEY = "image";
    public static String GEOPOINT_KEY = "geopoint";
    public static String USER_KEY = "user";

    public static int UPLOAD_IMAGE_MAX_WIDTH = 800;  // in pixels
    public static int UPLOAD_IMAGE_MAX_HEIGHT = 450; // in pixels
    public static int UPLOAD_IMAGE_MAX_SIZE = 45000;  // in bytes

    private KiiUser user;
    private SQLiteDatabase mDb;
    private DatabaseHandler mDbHelper;

    private Context context;

    public UploadReceiverKii()
    {
    }

    @Override
    public void onReceive(Context cont, Intent intent)
    {

        this.context = cont;

        // Initialize database access
        mDbHelper = new DatabaseHandler(context);
        mDb = mDbHelper.getWritableDatabase();

        // Test for internet connectivity
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();

        // If we are connected to internet, do all uploading.  Otherwise do nothing.
        if (canConnect(info))
        {
            Log.d("[Upload]", "Internet detected.");

            (new AsyncTask<String, String, String>()
            {
                @Override
                protected String doInBackground(String... arg0)
                {

                    // Retrieve all database elements that need to be submitted
                    Cursor dataCursor = mDb.query(DatabaseHandler.TABLE_NAME, DatabaseHandler.COLUMNS,
                                                  DatabaseHandler.SUBMITTED + " = '0'",
                                                  null, null, null, null);
                    Cursor trackCursor = mDb.query(DatabaseHandler.TRACK_TABLE_NAME,
                                                   DatabaseHandler.TRACK_COLUMNS,
                                                   null, null, null, null, null);

                    // If there are pending submissions, submit them.
                    if (dataCursor.getCount() > 0 || trackCursor.getCount() > 0)
                    {
                        Log.d("[Upload]", "Found " + dataCursor.getCount() + " data entries"
                                          + " and " + trackCursor.getCount() + " tracking entries to upload.");
                        logInUser(dataCursor, trackCursor);
                    }
                    else
                    {
                        Log.d("[Upload]", "No entries to upload.");
                    }
                    return null;
                }
            }).execute();
        }

    }
    private boolean canConnect(NetworkInfo info)
    {
        if (!info.isInternetConnected())
        {
            Log.d("[Upload]", "Upload failed, no internet connection detected.");
            return false;
        }

        if (GPSSnapshot.getStringPref(context, "upload_type")
                       .equals(context.getString(R.string.pref_3g_or_wifi))
            && info.isInternetConnected())
        {
            return true;
        }

        if (GPSSnapshot.getStringPref(context, "upload_type")
                       .equals(context.getString(R.string.pref_wifi_only))
            && info.isInternetConnected()
            && info.getType() == ConnectivityManager.TYPE_WIFI)
        {
            return true;
        }

        if (GPSSnapshot.getStringPref(context, "upload_type")
                       .equals(context.getString(R.string.pref_never)))
        {
            Log.d("[Upload]", "Uploading currently disabled.");
        }

        Log.d("[Upload]", "Upload conditions are not met, aborting upload.");
        return false;
    }

    private void logInUser(Cursor dataCursor, Cursor trackCursor)
    {

        Log.d("[Upload]", "Starting Kii connection and upload...");

        // Initialize Kii
        Kii.initialize(GPSSnapshot.KII_APPID, GPSSnapshot.KII_APPKEY,
                       GPSSnapshot.KII_SITE);

        // Get access token for this device's login
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String accessToken = settings.getString(GPSSnapshot.ACCESSTOKEN_KEY, null);

        // If no such token exists, create a new one and store it.
        if (accessToken == null)
        {
            try
            {
                user = KiiUser.registerAsPseudoUser(new UserFields());

                String username = GPSSnapshot.getStringPref(context, "display_name");
                if (username != null)
                    user.setDisplayname(username);

                if (user != null)
                {
                    accessToken = user.getAccessToken();
                    Log.d("[Kii]", "Logged in as new user.\nAccess token = " + accessToken
                                   + "\nUser ID = " + user.getID());
                }
                else
                {
                    Log.e("[Kii]", "Could not log in to Kii");
                    return;
                }

                SharedPreferences.Editor editor = settings.edit();
                editor.putString(GPSSnapshot.ACCESSTOKEN_KEY, accessToken);
                editor.commit();
            } catch (IOException e)
            {
                Log.e("[Kii]", "Could not create login.  Aborting network activities.\n"
                               + e.getMessage());
                e.printStackTrace();
                return;
            } catch (AppException e)
            {
                Log.e("[Kii]", "Could not create login.  Aborting network activities.\n"
                               + e.getMessage());
                e.printStackTrace();
                return;
            }
        }
        else // If the token exists, log in using it
        {
            try
            {
                KiiUser.loginWithToken(accessToken);

                user = KiiUser.getCurrentUser();

                if (user.getDisplayname() == null)
                {
                    String username = GPSSnapshot.getStringPref(context, "display_name");
                    if (username != null)
                        user.setDisplayname(username);
                }
                Log.d("[Kii]", "Logged in as existing user.\nAccess token = " + accessToken
                               + "\nUser ID = " + user.getID());

            } catch (IOException e)
            {
                Log.e("[Kii]", "Could not log in.  Aborting network activities.\n"
                               +e.getMessage());
                e.printStackTrace();
                return;
            } catch (AppException e)
            {
                Log.e("[Kii]", "Could not log in.  Aborting network activities.\n"
                               + e.getMessage());
                e.printStackTrace();
                return;
            }

        }


        // Iterate through all results
        while (dataCursor.moveToNext())
        {
            KiiObject object = Kii.bucket(GPSSnapshot.DATA_BUCKET).object();

            final String picPath = dataCursor.getString(dataCursor.getColumnIndex(DatabaseHandler.PIC_PATH));
            String time = dataCursor.getString(dataCursor.getColumnIndex(DatabaseHandler.TIME));
            float lati = dataCursor.getFloat(dataCursor.getColumnIndex(DatabaseHandler.LATITUDE));
            float longi = dataCursor.getFloat(dataCursor.getColumnIndex(DatabaseHandler.LONGITUDE));
            GeoPoint geoPoint = new GeoPoint(lati, longi);

            // Break down the date and time data into a JSON object with fields representing
            //  the different denominations of time
            Calendar calendar = Calendar.getInstance();
            try
            {
                calendar.setTime(GPSSnapshot.DATE_FORMAT.parse(time));

                JSONObject timeObject = new JSONObject();
                timeObject.put("year", calendar.get(Calendar.YEAR));
                timeObject.put("month", calendar.get(Calendar.MONTH));
                timeObject.put("day", calendar.get(Calendar.DAY_OF_MONTH));
                timeObject.put("hour", calendar.get(Calendar.HOUR_OF_DAY));
                timeObject.put("minute", calendar.get(Calendar.MINUTE));
                timeObject.put("second", calendar.get(Calendar.SECOND));

                object.set(TIME_KEY, timeObject);
            } catch (ParseException e)
            {
                Log.e("[Data]", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e)
            {
                Log.e("[Data]", e.getMessage());
                e.printStackTrace();
            }

            // Add Lat/Long Coords
            object.set(GEOPOINT_KEY, geoPoint);

            // Add user ID and name
            try {
                JSONObject userObject = new JSONObject();
                userObject.put("id", user.getID());
                userObject.put("name", user.getDisplayname());


                object.set(USER_KEY, userObject);
            } catch (JSONException e) {
                Log.e("[Data]", e.getMessage());
                e.printStackTrace();
            }

            // Load the image, convert it to PNG, and store
            //  the byte array.
            try
            {
                // Open the base image.
                File file = new File(picPath);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(file), null, options);

                // Resize the image as needed to fit in require width
                int sampleSize = options.outWidth / UPLOAD_IMAGE_MAX_WIDTH;
                if (sampleSize < 1) { sampleSize = 1; }
                options.inSampleSize = sampleSize;
                options.inPreferredConfig = Bitmap.Config.RGB_565;
                options.inJustDecodeBounds = false;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options);

                // Find the maximum quality that will fit inside the maximum file size by trying
                //  lower and lower quality until it fits.
                int compressStep = 10;  // Larger amount means faster, but less precise
                int compressQuality = 100 + compressStep;
                int streamLength = GPSSnapshot.getIntPref(context, "upload_size")*1024;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                while (streamLength >= GPSSnapshot.getIntPref(context, "upload_size")*1024
                       && compressQuality > compressStep)
                {
                    try
                    {
                        stream.flush();
                        stream.reset();
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    compressQuality -= compressStep;
                    bitmap.compress(Bitmap.CompressFormat.JPEG, compressQuality, stream);
                    byte[] byteArray = stream.toByteArray();
                    streamLength = byteArray.length;
                }

                if (compressQuality > 0)
                {
                    Log.d("[Image Rescaling]", "Image scaled to " + streamLength + " bytes, " +
                                               "at quality " + compressQuality);
                    object.set(PIC_KEY, stream.toByteArray());
                }
                else
                {
                    Log.d("[Image Rescaling]", "Unable to scale image to appropriate size.");
                }

                // Save the object, and if successful, report success and mark data point as submitted.
                object.save(new KiiObjectCallBack()
                {
                    @Override
                    public void onSaveCompleted(int token, KiiObject object, Exception exception)
                    {
                        if (exception != null)
                        {
                            Log.e("[Data]", exception.getMessage());
                            exception.printStackTrace();
                        }
                        else
                        {
                            // Mark entry in database as having been submitted, and attempt to
                            ContentValues val = new ContentValues();
                            val.put(DatabaseHandler.SUBMITTED, 1);
                            mDb.update(DatabaseHandler.TABLE_NAME, val, DatabaseHandler.PIC_PATH
                                                                        + " = '" + picPath + "'", null);
                            Log.d("[Upload]", "Image data upload successful.\n" + picPath);
                        }
                    }
                });

            } catch (IOException e)
            {
                Log.e("[Data]", e.getMessage());
            }
        }

        // If image data was submitted, attempt to clean picture folder in case the current setting
        // is to delete upon upload.
        if (dataCursor.getCount() > 0)
            GPSSnapshot.cleanPicturesFolder(context);

        while (trackCursor.moveToNext())
        {
            KiiObject object = Kii.bucket(GPSSnapshot.TRACKING_BUCKET).object();

            final String time = trackCursor.getString(trackCursor.getColumnIndex(
                    DatabaseHandler.TRACK_TIME));
            float lati = trackCursor.getFloat(trackCursor.getColumnIndex(
                    DatabaseHandler.TRACK_LATITUDE));
            float longi = trackCursor.getFloat(trackCursor.getColumnIndex(
                    DatabaseHandler.TRACK_LONGITUDE));
            GeoPoint geoPoint = new GeoPoint(lati, longi);

            // Break down the date and time data into a JSON object with fields representing
            //  the different denominations of time
            Calendar calendar = Calendar.getInstance();
            try
            {
                calendar.setTime(GPSSnapshot.DATE_FORMAT.parse(time));

                JSONObject timeObject = new JSONObject();
                timeObject.put("year", calendar.get(Calendar.YEAR));
                timeObject.put("month", calendar.get(Calendar.MONTH));
                timeObject.put("day", calendar.get(Calendar.DAY_OF_MONTH));
                timeObject.put("hour", calendar.get(Calendar.HOUR_OF_DAY));
                timeObject.put("minute", calendar.get(Calendar.MINUTE));
                timeObject.put("second", calendar.get(Calendar.SECOND));

                object.set(TIME_KEY, timeObject);
            } catch (ParseException e)
            {
                Log.e("[Data]", e.getMessage());
                e.printStackTrace();
            } catch (JSONException e)
            {
                Log.e("[Data]", e.getMessage());
                e.printStackTrace();
            }

            // Add Lat/Long coordinates
            object.set(GEOPOINT_KEY, geoPoint);

            // Add user ID and name
            try {
                JSONObject userObject = new JSONObject();
                userObject.put("id", user.getID());
                userObject.put("name", user.getDisplayname());

                object.set(USER_KEY, userObject);
            } catch (JSONException e) {
                Log.e("[Data]", e.getMessage());
                e.printStackTrace();
            }

            // Save the object, and if successful, report success and delete tracking entry.
            object.save(new KiiObjectCallBack()
            {
                @Override
                public void onSaveCompleted(int token, KiiObject object, Exception exception)
                {
                    if (exception != null)
                    {
                        Log.e("[Upload]", exception.getMessage());
                        exception.printStackTrace();
                    }
                    else
                    {
                        // Delete tracking entry in database
                        mDb.delete(DatabaseHandler.TRACK_TABLE_NAME,
                                   DatabaseHandler.TRACK_TIME + " = " + time,
                                   null);

                        Log.d("[Upload]", "Tracking data upload successful.");
                    }
                }
            });
        }


    }
    }
    */


