package com.sprelf.gpssnapshot.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sprelf.gpssnapshot.GPSSnapshot;

public class PictureCleanupReceiver extends BroadcastReceiver
{
    public PictureCleanupReceiver()
    {
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d("[Image Cleanup]", "Starting to check for pictures to delete.");
        GPSSnapshot.cleanPicturesFolder(context);
    }
}
