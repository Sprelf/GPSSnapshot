package com.sprelf.gpssnapshot.Receivers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;

import org.json.JSONObject;

public class ParseNotificationReceiver extends ParsePushBroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent)
    {
        try
        {
            String jsonData = intent.getExtras().getString("com.parse.Data");
            JSONObject json = new JSONObject(jsonData);



            String title = null;
            if (json.has("title"))
            {
                title = json.getString("title");
            }

            String message = null;
            if (json.has("alert"))
            {
                message = json.getString("alert");
            }

            String uri = null;
            if (json.has("uri"))
            {
                uri = json.getString("uri");
            }

            Log.d("[Push]", "Push notification received: "+uri);

            if (message != null)
            {
                generateNotification(context, title, message, uri);
            }
        } catch (Exception e)
        {
            Log.e("NOTIF ERROR", e.toString());
        }
    }


    private void generateNotification(Context context, String title, String message, String uri)
    {
        if (!GPSSnapshot.getBoolPref(context, "notifications_enabled"))
            return;

        Uri soundUri = (GPSSnapshot.getBoolPref(context, "notifications_sound"))
                ? Uri.parse("android.resource://" + context.getPackageName()
                            + "/" + R.raw.alert_thats_how_it_goes)
                : RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, 0);

        NotificationManager mNotifM =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (title == null)
        {
            title = context.getResources().getString(R.string.app_name);
        }

        final NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.logo_cent)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                                                   R.drawable.logo_cent))
                        .setContentTitle(title)
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                          .bigText(message))
                        .setContentIntent(contentIntent)
                        .setAutoCancel(true)
                        .setDefaults(new NotificationCompat().DEFAULT_VIBRATE)
                        .setSound(soundUri);

        mBuilder.setContentIntent(contentIntent);

        int notifId = GPSSnapshot.getIntPref(context, "notification_id");
        if (notifId >= 256) notifId = 0;
        GPSSnapshot.setIntPref(context, "notification_id", notifId+1);

        mNotifM.notify(notifId, mBuilder.build());
    }
}
