package com.sprelf.gpssnapshot.UtilityClasses;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.sprelf.gpssnapshot.GPSSnapshot;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Chris on 24.10.2015.
 */
public class ImageHandler
{

    private byte[] data;
    private String storageName;
    private boolean isStored;
    private Context c;

    public ImageHandler(Context c)
    {
        this.c = c;
        data = new byte[0];
    }

    public ImageHandler(Context c, byte[] data)
    {
        this.c = c;
        setData(data);
    }

    public void setData(byte[] data)
    {
        if (data!=null)
            this.data = data;
        else
            this.data = new byte[0];
    }

    public byte[] getData()
    {
        if (isStored)
        {
            retrieveImage();
        }
        if (isStored)
            data = new byte[0];
        return data;
    }

    public Bitmap getImage()
    {
        // If the image is stored, attempt to pull it out of storage
        if (isStored)
        {
            retrieveImage();
        }
        // If image was not stored or if retrieval succeeded, return the decoded data.
        //  Otherwise, return null.
        return isStored ? null : BitmapFactory.decodeByteArray(data, 0, data.length);
    }


    /**
     * Stores the byte array 'data' into a temporary file, and saves the temporary file's name in
     * 'storageName'.
     */
    public void storeImage()
    {
        // If the image is already stored, don't bother
        if (isStored)
            return;

        try
        {
            File file = File.createTempFile("image", null, c.getCacheDir());
            storageName = file.getAbsolutePath();

            FileOutputStream writer = new FileOutputStream(file);
            writer.write(data);
            writer.flush();
            writer.close();

            data = null;
            isStored = true;
            Log.d("[Image Storage]", "Successfully stored image.");
        } catch (IOException e)
        {
            Log.e("[Image Storage]", "Error storing image in external cache.");
            e.printStackTrace();
        }
    }

    /** Called to deconstruct this object, and delete any temporary files associated with it.
     *
     */
    public void destroy()
    {
        if (isStored)
        {
            File file = new File(storageName);

            if (!file.delete())
                Log.e("[Image Storage]", "Failed to delete image file from storage.");
        }
        data = null;
    }


    /** Pulls the given ParseFile's data from Parse and sets this Image Handler's data
     * to the result.
     *
     * @param file Parse File to query.
     * @param response Methods to run upon success or failure.  Can be null.
     */
    private void queryImage(ParseFile file, final ImageResponse response)
    {
        // Perform image data query
        file.getDataInBackground(new GetDataCallback()
        {
            @Override
            public void done(byte[] bytes, ParseException e)
            {
                if (e != null)
                {
                    Log.e("[Image Query]", "Error retrieving image data.\n" + e.getMessage() +
                                           "\nError Code = " + e.getCode());
                    GPSSnapshot.generateErrorDialog(c, GPSSnapshot.ERROR_PARSE, null);
                    if (response!=null)
                        response.onFailure(e.getCode());
                }
                else
                {
                    // Put the acquired image data into the image handler
                    setData(bytes);
                    if (response!=null)
                        response.onSuccess();
                }
            }
        });
    }


    /**
     * Retrieves byte array from storage and puts it in the variable 'data'.
     */
    private void retrieveImage()
    {
        data = null;
        try
        {
            File file = new File(storageName);
            data = new byte[(int) file.length()];
            DataInputStream dis = new DataInputStream(new FileInputStream(file));
            dis.readFully(data);
            dis.close();

            if (!file.delete()) // delete the file and check for response
            {
                Log.e("[Image Storage]", "Error deleting retrieved cache file.");
            }
            else
            {
                Log.d("[Image Storage]", "Successfully retrieved image from storage.");
                isStored = false;  // file successfully pulled from storage
                return;
            }
        } catch (FileNotFoundException e)
        {
            Log.e("[Image Storage]", "Stored image not found.");
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        isStored = true; // only called if something failed.
    }




    /** Creates a new image handler and immediately queries for data from Parse.
     *
     * @param c Context within which to perform the operation
     * @param file ParseFile to query
     * @return The new {@link ImageHandler}
     */
    public static ImageHandler newFromParse(Context c, ParseFile file)
    {
        final ImageHandler returnImage = new ImageHandler(c);

        returnImage.queryImage(file, null);

        return returnImage;
    }

    /** Creates a new image handler and immediately queries for data from Parse.
     *
     * @param c Context within which to perform the operation
     * @param file ParseFile to query
     * @param response Methods to run upon success or failure.  Can be null.
     * @return The new {@link ImageHandler}
     */
    public static ImageHandler newFromParse(Context c, ParseFile file, ImageResponse response)
    {
        final ImageHandler returnImage = new ImageHandler(c);

        returnImage.queryImage(file, response);

        return returnImage;
    }
}
