package com.sprelf.gpssnapshot.UtilityClasses;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Chris on 20.10.2015.
 */
public class SlidingAnimation extends Animation
{
    private int startX;
    private int deltaX;
    private View view;

    public SlidingAnimation(View v)
    {
        this.view = v;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t)
    {
        view.setX(startX + (deltaX * interpolatedTime));
    }

    public void setParams(int start, int end)
    {
        this.startX = start;
        deltaX = end - startX;
    }

    @Override
    public boolean willChangeBounds()
    {
        return true;
    }
}
