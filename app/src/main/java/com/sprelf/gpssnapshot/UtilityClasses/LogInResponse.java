package com.sprelf.gpssnapshot.UtilityClasses;

/**
 * Created by Chris on 27.10.2015.
 */
public interface LogInResponse
{
    public void onSuccess();
    public void onInvalid();
    public void onFailure(int errorCode);
}
