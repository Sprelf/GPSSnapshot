package com.sprelf.gpssnapshot.UtilityClasses;

import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by Chris on 21.10.2015.
 */
public class GroupResults
{
    public String name;
    public ArrayList<ParseObject> objects;
    public ArrayList<ImageHandler> images;

    public GroupResults(String name, ArrayList<ParseObject> objects, ArrayList<ImageHandler> images)
    {
        setData(name, objects, images);
    }

    /** Store all of the given info into this object.
     * IMPORTANT:  DOES NOT COPY A REFERENCE OF THE GIVEN ARRAYLISTS, BUT INSTEAD COPIES THE
     * CONTENTS.  FUTURE CHANGES TO THE GIVEN ARRAYS WILL NOT AFFECT THIS OBJECT.
     *
     * @param name Name of the group
     * @param objects List of Parse Objects to store
     * @param images List of Images to store
     */
    public void setData(String name, ArrayList<ParseObject> objects, ArrayList<ImageHandler> images)
    {
        this.name = name;
        this.objects = new ArrayList<>();
        this.objects.addAll(objects);
        this.images = new ArrayList<>();
        this.images.addAll(images);
    }
}
