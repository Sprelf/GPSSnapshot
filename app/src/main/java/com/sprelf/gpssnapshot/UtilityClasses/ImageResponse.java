package com.sprelf.gpssnapshot.UtilityClasses;

/**
 * Created by Chris on 27.10.2015.
 */
public interface ImageResponse
{
    public void onSuccess();
    public void onFailure(int errorCode);
}
