package com.sprelf.gpssnapshot.UtilityClasses;

import android.database.Cursor;

import com.parse.ParseObject;
import com.parse.ParseUser;
import com.sprelf.gpssnapshot.Handlers.DatabaseHandler;

public class DataGroup
{
    public static final String GROUP_NAME = "group_name";
    public static final String DESCRIPTION = "description";
    public static final String ACCESS_CODE = "access_code";
    public static final String DB_APP_ID = "db_app_id";
    public static final String DB_CLIENT_KEY = "db_client_key";
    public static final String NOTIFY_URGENT_ONLY = "notify_urgent_only";
    public static final String PUBLIC_ENTRIES = "public_entries";
    public static final String ADMIN = "admin";

    public String title;
    public String description;
    public String accessCode;
    public String appId;
    public String clientKey;
    public boolean isAdmin, publicEntries, notifyUrgentOnly;

    public DataGroup()
    {

    }

    public DataGroup(ParseObject obj)
    {
        constructFromParseObject(obj);
    }

    /** Pulls data from the given Parse Object to put into this object.
     *
     * @param obj Parse Object to pull data from.
     */
    public void constructFromParseObject(ParseObject obj)
    {
        title = obj.getString(GROUP_NAME);
        description = obj.getString(DESCRIPTION);
        accessCode = obj.getString(ACCESS_CODE);
        appId = obj.getString(DB_APP_ID);
        clientKey = obj.getString(DB_CLIENT_KEY);
        notifyUrgentOnly = obj.getBoolean(NOTIFY_URGENT_ONLY);
        publicEntries = obj.getBoolean(PUBLIC_ENTRIES);

        if (ParseUser.getCurrentUser()!=null)
            isAdmin = obj.getParseUser(ADMIN).hasSameId(ParseUser.getCurrentUser());
    }

    /** Returns the info in this object as a new Parse Object.
     *
     * @param admin Parse User to set as the admin of the returned Parse Object
     * @return This object as a Parse Object
     */
    public ParseObject asParseObject(ParseUser admin)
    {
        ParseObject obj = new ParseObject("DataGroup");
        obj.put(GROUP_NAME, title);
        obj.put(DESCRIPTION, description);
        obj.put(ACCESS_CODE, accessCode);
        obj.put(DB_APP_ID, appId);
        obj.put(DB_CLIENT_KEY, clientKey);
        obj.put(ADMIN, admin);
        obj.put(NOTIFY_URGENT_ONLY, notifyUrgentOnly);
        obj.put(PUBLIC_ENTRIES, publicEntries);

        return obj;
    }

    public static DataGroup fromSQLEntries(Cursor cursor)
    {
        DataGroup newGroup = new DataGroup();
        newGroup.title = cursor.getString(cursor.getColumnIndex(
                DatabaseHandler.GROUP_TITLE));
        newGroup.description = cursor.getString(cursor.getColumnIndex(
                DatabaseHandler.GROUP_DESCRIPTION));
        newGroup.accessCode = cursor.getString(cursor.getColumnIndex(
                DatabaseHandler.GROUP_ACCESS_CODE));
        newGroup.appId = cursor.getString(cursor.getColumnIndex(
                DatabaseHandler.GROUP_DB_APPID));
        newGroup.clientKey = cursor.getString(cursor.getColumnIndex(
                DatabaseHandler.GROUP_DB_CLIENTKEY));
        newGroup.isAdmin = (cursor.getInt(cursor.getColumnIndex(
                DatabaseHandler.GROUP_IS_ADMIN)) == 1);
        newGroup.publicEntries = (cursor.getInt(cursor.getColumnIndex(
                DatabaseHandler.GROUP_IS_PUBLIC_ENTRIES)) == 1);

        return newGroup;
    }

    @Override
    public String toString()
    {
        return String.format("%s, %s\n%s [PE=%s, NUO=%s]",
                             title,
                             description,
                             (isAdmin ? "ADMIN" : "NOT ADMIN"),
                             (publicEntries ? "true" : "false"),
                             (notifyUrgentOnly ? "true" : "false"));
    }
}
