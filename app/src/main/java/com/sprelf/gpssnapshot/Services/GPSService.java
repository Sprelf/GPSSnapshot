package com.sprelf.gpssnapshot.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.sprelf.gpssnapshot.GPSSnapshot;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GPSService extends Service
{
    private LocationManager locationManager;

    private static final ScheduledExecutorService executor =
            Executors.newScheduledThreadPool(5);

    private String picPath, time, description, groupName;
    private boolean isUrgent;
    private ArrayList<LocationListener> listeners;
    private Location storedLocation;

    public GPSService()
    {
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    /** Handles polling for GPS location in the background, and then logs all data once polling
     * concludes.
     *
     * @param intent Intent passed to this object, contains photo details
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        listeners = new ArrayList<>();

        if (intent != null)
        {
            // Retrieve pic path and time data
            picPath = intent.getStringExtra("picPath");
            time = intent.getStringExtra("time");
            description = intent.getStringExtra("description");
            groupName = intent.getStringExtra("groupName");
            isUrgent = intent.getBooleanExtra("urgent", false);

            // Initialize location manager
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            // Determine the best current network provider
            String bestProvider;
            if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            {
                ConnectivityManager cm =
                        (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                                      activeNetwork.isConnected();

                bestProvider = isConnected ? LocationManager.NETWORK_PROVIDER :
                        LocationManager.GPS_PROVIDER;
            }
            else if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            {
                Log.d("[GPS]", "No location service is enabled.");
                // TODO:  HANDLE ERROR
                return -1;
            }
            else
            {
                bestProvider = LocationManager.GPS_PROVIDER;
            }

            Log.d("[GPS]", "Utilizing '"+bestProvider+"' as the location provider.");

            performPolling(bestProvider);

        }

        return START_STICKY;
    }

    private void performPolling(final String provider)    {

        final Location location = new Location(LocationManager.GPS_PROVIDER);
        location.setAccuracy(GPSSnapshot.NULL_GPS);
        LocationListener locationListener = new LocationListener()
        {
            private long startTime = 0;
            @Override
            public void onLocationChanged(Location newLocation)
            {
                if (startTime == 0)
                {
                    startTime = System.currentTimeMillis();
                }
                else
                {
                    // This is just a failsafe
                    if ((System.currentTimeMillis()-startTime)/1000 >
                        GPSSnapshot.getIntPref(getApplicationContext(), "image_timeout")*2)
                    {
                        Log.d("[GPS]", "Failsafe cancelling mechanism engaged.");
                        storeData();
                    }
                }


                location.set(newLocation);
                Log.d("[GPS]", "Location poll received.  Accuracy = "+location.getAccuracy());
                if (location.getAccuracy() <=
                    GPSSnapshot.getIntPref(getApplicationContext(), "image_accuracy"))
                {
                    storeData();
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras)
            {

            }

            @Override
            public void onProviderEnabled(String provider)
            {

            }

            @Override
            public void onProviderDisabled(String provider)
            {

            }

            private void storeData()
            {
                GPSSnapshot.addDataEntry(getApplicationContext(), picPath, time,
                                         description, groupName, isUrgent, location);
                locationManager.removeUpdates(this);
                listeners.remove(this);
                stopSelf();
            }
        };
        listeners.add(locationListener);


        Log.d("[GPS]", "Starting location polling...");

        // Start polling for location updates and attach the above listener
        locationManager.requestLocationUpdates(provider,
                                               GPSSnapshot.GPS_POLLING_FREQ_DEFAULT,
                                               0,
                                               locationListener);

        // Delayed runnable to kill GPS if it can't resolve.
        executor.schedule(new Runnable()
        {
            @Override
            public void run()
            {
                removeAllListeners();
                if (location.getAccuracy() == GPSSnapshot.NULL_GPS)
                {
                    Log.d("[GPS]", "Could not resolve location.");


                    // storedLocation will be null if this is the first location polling attempt
                    GPSSnapshot.addDataEntry(getApplicationContext(), picPath, time,
                                             description, groupName, isUrgent, storedLocation);
                    stopSelf();
                }
                else if (location.getAccuracy() >
                         GPSSnapshot.getIntPref(getApplicationContext(), "image_accuracy"))
                {
                    if (provider.equals(LocationManager.GPS_PROVIDER))
                    {

                        Log.d("[GPS]", "Could not get accurate measurement.  Settled for "
                                       + Float.toString(location.getAccuracy()) + "m.");
                        GPSSnapshot.addDataEntry(getApplicationContext(), picPath, time,
                                                 description, groupName, isUrgent, location);
                        stopSelf();
                    }
                    else
                    {
                        Log.d("[GPS]", "Could not get accurate measurement with network provider." +
                                       "  Attempting to use GPS instead.");
                        Looper.prepare();
                        storedLocation = location;
                        performPolling(LocationManager.GPS_PROVIDER);
                        Looper.loop();
                    }

                }
            }
        }, GPSSnapshot.getIntPref(getApplicationContext(), "image_timeout"), TimeUnit.SECONDS);


    }

    @Override
    public void onDestroy()
    {
        Log.d("[GPS]", "GPS service destroyed.");
        removeAllListeners();
        super.onDestroy();
    }

    private void removeAllListeners()
    {
        for (LocationListener l : listeners)
        {
            locationManager.removeUpdates(l);
        }
    }

}


