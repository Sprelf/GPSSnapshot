package com.sprelf.gpssnapshot.Services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.sprelf.gpssnapshot.GPSSnapshot;

public class AppCloseService extends Service
{
    public AppCloseService()
    {
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent intent)
    {
        GPSSnapshot.stopTrackingService(this);
    }
}
