package com.sprelf.gpssnapshot.Handlers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Chris on 08.09.2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper
{

    public final static String TABLE_NAME = "image_data";
    public final static String PIC_PATH = "pic_path";
    public final static String TIME = "data_time";
    public final static String LATITUDE = "data_latitude";
    public final static String LONGITUDE = "data_longitude";
    public final static String DESCRIPTION = "data_description";
    public final static String GROUP_NAME = "data_group_name";
    public final static String IS_URGENT = "data_is_urgent";
    public final static String SUBMITTED = "data_submitted";
    public final static String[] COLUMNS = {PIC_PATH, TIME, LATITUDE, LONGITUDE, DESCRIPTION,
                                            GROUP_NAME, IS_URGENT, SUBMITTED};

    public final static String TRACK_TABLE_NAME = "tracking_data";
    public final static String TRACK_TIME = "track_time";
    public final static String TRACK_LATITUDE = "track_latitude";
    public final static String TRACK_LONGITUDE = "track_longitude";
    public final static String[] TRACK_COLUMNS = {TRACK_TIME, TRACK_LATITUDE, TRACK_LONGITUDE};

    public final static String GROUP_TABLE_NAME = "group_data";
    public final static String GROUP_TITLE = "group_title";
    public final static String GROUP_DESCRIPTION = "group_description";
    public final static String GROUP_ACCESS_CODE = "group_access_code";
    public final static String GROUP_DB_APPID = "group_app_id";
    public final static String GROUP_DB_CLIENTKEY = "group_client_key";
    public final static String GROUP_IS_ADMIN = "group_is_admin";
    public final static String GROUP_IS_PUBLIC_ENTRIES = "group_is_public_entries";
    public final static String[] GROUP_COLUMNS = {GROUP_TITLE, GROUP_DESCRIPTION, GROUP_ACCESS_CODE,
                                                  GROUP_DB_APPID, GROUP_DB_CLIENTKEY, GROUP_IS_ADMIN,
                                                  GROUP_IS_PUBLIC_ENTRIES};

    final private static String NAME = "image_db";
    final private static Integer VERSION = 1;
    final private Context mContext;

    public DatabaseHandler(Context context)
    {
        super(context, NAME, null, VERSION);
        this.mContext = context;
    }

    final private static String CREATE_CMD_DATA =

            "CREATE TABLE " + TABLE_NAME + " ("
            + PIC_PATH + " TEXT PRIMARY KEY, "
            + TIME + " TEXT, "
            + LATITUDE + " FLOAT, "
            + LONGITUDE + " FLOAT, "
            + DESCRIPTION + " TEXT, "
            + GROUP_NAME + " TEXT, "
            + IS_URGENT + " INTEGER, "
            + SUBMITTED + " INTEGER)";

    final private static String CREATE_CMD_DATA2 =

            "CREATE TABLE " + TRACK_TABLE_NAME + " ("
            + TRACK_TIME + " TEXT PRIMARY KEY, "
            + TRACK_LATITUDE + " FLOAT, "
            + TRACK_LONGITUDE + " FLOAT)";

    final public static String CREATE_CMD_DATA3 =

            "CREATE TABLE " + GROUP_TABLE_NAME + " ("
            + GROUP_TITLE + " TEXT PRIMARY KEY, "
            + GROUP_DESCRIPTION + " TEXT, "
            + GROUP_ACCESS_CODE + " TEXT, "
            + GROUP_DB_APPID + " TEXT, "
            + GROUP_DB_CLIENTKEY + " TEXT, "
            + GROUP_IS_ADMIN + " INTEGER, "
            + GROUP_IS_PUBLIC_ENTRIES + " INTEGER)";


    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_CMD_DATA);
        db.execSQL(CREATE_CMD_DATA2);
        db.execSQL(CREATE_CMD_DATA3);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // TODO Auto-generated method stub

    }
}
