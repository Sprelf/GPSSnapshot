package com.sprelf.gpssnapshot.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.parse.ParseObject;
import com.sprelf.gpssnapshot.CustomViews.MapWrapperLayout;
import com.sprelf.gpssnapshot.GPSSnapshot;
import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.Receivers.UploadReceiver;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;
import com.sprelf.gpssnapshot.UtilityClasses.ImageHandler;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Chris on 10.10.2015.
 */
public class DataWindowAdapter implements GoogleMap.InfoWindowAdapter
{
    private View windowView;
    List<Marker> markers;
    List<ParseObject> objects;
    List<ImageHandler> images;
    MapWrapperLayout mapWrapper;
    Context c;

    public DataWindowAdapter(Context c, List<Marker> markers, List<ParseObject> objects,
                             List<ImageHandler> images, MapWrapperLayout mapWrapper)
    {
        if (c.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            windowView = View.inflate(c, R.layout.layout_data_info_window, null);
        else
            windowView = View.inflate(c, R.layout.layout_data_info_window_land, null);
        windowView.setLayoutParams(new RelativeLayout.LayoutParams(
                c.getResources().getDimensionPixelSize(R.dimen.MapViewer_InfoWindowWidth),
                c.getResources().getDimensionPixelSize(R.dimen.MapViewer_InfoWindowHeight)));

        this.markers = markers;
        this.objects = objects;
        this.images = images;
        this.mapWrapper = mapWrapper;
        this.c = c;
    }


    @Override
    public View getInfoWindow(Marker marker)
    {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker)
    {
        mapWrapper.setMarkerWithInfoWindow(marker, windowView);

        // Identify which marker is being opened
        int index = markers.indexOf(marker);

        ParseObject object = objects.get(index);

        // Set image
        ((ImageView) windowView.findViewById(R.id.ImageViewer_ImageArea))
                .setImageBitmap(images.get(index).getImage());


        // Set time text

        ((TextView) windowView.findViewById(R.id.ImageViewer_TimeText))
                .setText(new SimpleDateFormat("dd MMM, yyyy HH:mm:ss").format(
                        object.getDate(UploadReceiver.TIME_KEY)));

        // Set Lat/Long text
        NumberFormat numFormat = new DecimalFormat();
        numFormat.setMaximumFractionDigits(6);
        String gpsText = String.format("[%s, %s]",
                                       numFormat.format(
                                               object.getParseGeoPoint("geopoint").getLatitude()),
                                       numFormat.format(
                                               object.getParseGeoPoint("geopoint").getLongitude()));
        ((TextView) windowView.findViewById(R.id.ImageViewer_GPSText))
                .setText(gpsText);

        // Set display name text
        StringBuilder nameBuilder = new StringBuilder();
        if (object.getBoolean(UploadReceiver.URGENT_KEY))
            nameBuilder.append(c.getString(R.string.MapViewer_UrgentLabel) + " ");
        nameBuilder.append(object.getString(UploadReceiver.USER_NAME_KEY));
        ((TextView) windowView.findViewById(R.id.ImageViewer_DisplayNameText))
                .setText(nameBuilder.toString());

        // Set description text
        ((TextView) windowView.findViewById(R.id.ImageViewer_DescriptionText))
                .setText(object.getString(UploadReceiver.DESCRIPTION_KEY));

        // Enable/disable phone button
        DataGroup currGroup = GPSSnapshot.getCurrentGroup(c);
        ImageView buttonView =
                (ImageView) windowView.findViewById(R.id.ImageViewer_PhoneButton);
        if (currGroup != null &&
            currGroup.title.equals(object.getString(UploadReceiver.GROUP_NAME_KEY)) &&
            currGroup.isAdmin &&
            object.containsKey(UploadReceiver.USER_PHONE_KEY))
        {

            final String phoneNumber = object.getString(UploadReceiver.USER_PHONE_KEY);
            buttonView.setImageDrawable(c.getResources().getDrawable(R.drawable.call_contact));
            buttonView.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent ev)
                {
                    Log.d("[Map]", "Phone number button clicked.  Phone Number = " + phoneNumber);
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                    c.startActivity(intent);
                    return true;
                }
            });

        }
        else
        {
            buttonView.setImageDrawable(c.getResources().getDrawable(R.drawable.call_contact_grey));
            buttonView.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    return false;
                }
            });
        }


        return windowView;
    }
}
