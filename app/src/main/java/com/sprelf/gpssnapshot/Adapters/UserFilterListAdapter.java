package com.sprelf.gpssnapshot.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sprelf.gpssnapshot.R;

import java.util.HashMap;

public class UserFilterListAdapter extends BaseAdapter
{

    private Context c;
    private HashMap<String, Boolean> objects;
    private String[] keys;

    public UserFilterListAdapter(Context c, HashMap<String, Boolean> objects)
    {
        keys = objects.keySet().toArray(new String[objects.size()]);

        this.c = c;
        this.objects = objects;
    }

    @Override
    public int getCount()
    {
        return objects.size();
    }

    @Override
    public Object getItem(int position)
    {
        return objects.get(keys[position]);
    }

    public String getKey(int position)
    {
        return keys[position];
    }

    @Override
    public long getItemId(int arg0)
        {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater =
                (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View groupView = inflater.inflate(R.layout.layout_user_filter_box, parent, false);

        TextView name = (TextView)groupView.findViewById(R.id.UserFilterBox_Text);
        CheckBox checkBox = (CheckBox)groupView.findViewById(R.id.UserFilterBox_CheckBox);

        name.setText(keys[position]);
        checkBox.setChecked(objects.get(keys[position]));

        return groupView;
    }

    @Override
    public void notifyDataSetChanged()
    {
        if (objects!=null)
            keys = objects.keySet().toArray(new String[objects.size()]);
        super.notifyDataSetChanged();
    }




}

