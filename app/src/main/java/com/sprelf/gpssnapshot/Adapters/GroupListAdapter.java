package com.sprelf.gpssnapshot.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sprelf.gpssnapshot.R;
import com.sprelf.gpssnapshot.UtilityClasses.DataGroup;

import java.util.ArrayList;

public class GroupListAdapter extends ArrayAdapter<DataGroup>
{

    private Context c;
    private ArrayList<DataGroup> objects;
    private boolean showAdd;

    public GroupListAdapter(Context c, ArrayList<DataGroup> objects, boolean showAdd)
    {
        super(c, R.layout.layout_group_info, objects);

        this.c = c;
        this.objects = objects;
        this.showAdd = showAdd;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater =
                (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View groupView = inflater.inflate(R.layout.layout_group_info, parent, false);

        attachGroupValsToView(groupView, objects.get(position));

        return groupView;
    }

    public static void attachGroupValsToView(View groupView, DataGroup data)
    {
        TextView titleText = (TextView)groupView.findViewById(R.id.GroupInfo_GroupTitle);
        TextView descripText = (TextView)groupView.findViewById(R.id.GroupInfo_GroupDescription);
        TextView adminText = (TextView)groupView.findViewById(R.id.GroupInfo_AdminBox);
        TextView addText = (TextView)groupView.findViewById(R.id.GroupInfo_AddButton);

        titleText.setText(data.title);
        descripText.setText(data.description);
        if (data.isAdmin)
            adminText.setVisibility(View.VISIBLE);
        //  if (showAdd)
        //      addText.setVisibility(View.VISIBLE);
    }

}